package com.shtibel.fm90.webservice


object APIs {

    // private static final String DOMAIN = "http://php2.shaligraminfotech.com/";//LOCAL
    //private static final String DOMAIN = "http://35.239.242.42/";//LIVE
    val BASE_URL = "https://www.90fm.co.il/"//LIVE
    // private val BASE_DOMAIN = DOMAIN + "/api/"
//    val BASE_URL = DOMAIN + "ki-api/"
    val BASE = "ki-api/"
    val BASE_IMAGE_PATH = BASE_URL+ BASE + "storage/"

    //Authentication
    val API_LOGIN = "login/app"
    val API_SIGNUP = "auth/signup"
    val API_UPDATE_PROFILE = "profile/update"
    val API_CHANGE_PASSWORD = "profile/change-pass"
    val API_FORGOT_PASSWORD = "forgot-password"
    val API_RESET_PASSWORD = "reset-password"

    val API_EDIT_PROFILE = "edit_profile"
    val API_EDIT_PROFILE_IMAGE = "edit_profile_image"
    val API_LOGOUT = "logout"
    val API_PRODUCT_LIST = "shoelist"
    val API_WATCH_LIST = "watch_list"
    val API_SEARCH_PRODUCT_LIST = "search_shoe"
    val API_MONTH_LIST = "month_list"
    val API_ADD_TO_WATCH_LIST = "add_to_watch_list"
    val API_PRODUCT_DETAILS = "shoe_detail"
    val API_COMMON = "get_page"
    val API_ADD_TO_LIKE_AND_DISLIKE = "add_to_like_dislike_list"
    val API_NOTIFICATION = "notification_list"
    val API_NOTIFICATION_COUNT = "unread_notification_count"
    val API_NOTIFICATION_COUNT_REMOVE = "clear_notification_count"
    val API_CLEAR_NOTIFICATION = "clear_notifications"
    val API_CLUB_LIST = "member/clubs"
    val API_GET_PROFILE = "profile/get"
    val API_TASK_LIST = "editors/tasks"
    val API_MY_TASK_LIST = "editors/tasks/approved"
    val API_TASK_DETAILS = "editors/tasks/detail"
    val API_TASK_APPROVE_REJECT = "clubs/member/approve-reject/tasks"


    val API_REQUEST_RESET_PASSWORD = "auth/request/reset-password"
    val API_REFRESH_TOKEN = "auth/update/device-token"
    val API_VERIFY_OTP = "auth/login/verify/otp"

}