package com.shtibel.fm90.receiver

import android.content.*
import android.media.session.PlaybackState
import android.os.IBinder
import com.shtibel.fm90.model.ListBeans
import com.shtibel.fm90.player.RadioService
import com.shtibel.fm90.player.RadioService.LocalBinder
import com.shtibel.fm90.util.SessionManager
import com.shtibel.fm90.util.Utils
import java.util.*

class NextProgramReceiver : BroadcastReceiver() {
    private lateinit var mTodaysProgramList: ArrayList<ListBeans>
    private lateinit var selectedListBeans: ListBeans

    override fun onReceive(context: Context?, intent: Intent?) {
        val sessionManager = SessionManager(context!!)
        mTodaysProgramList = Utils.getData(sessionManager.broadcastScheduleListModel)
        for (i in 0 until mTodaysProgramList.size) {
            selectedListBeans = mTodaysProgramList.get(i);
            if (Utils.isTimeInbetween(
                    selectedListBeans.program_start_time!!,
                    selectedListBeans.program_end_time
                )
            ) {
                selectedListBeans = mTodaysProgramList.get(1);
                context.applicationContext.bindService(
                    Intent(context, RadioService::class.java),
                    serviceConnection,
                    Context.BIND_AUTO_CREATE
                )
            }
        }
    }

    private val serviceConnection: ServiceConnection = object : ServiceConnection {
        override fun onServiceConnected(arg0: ComponentName, binder: IBinder) {
            val service = (binder as LocalBinder).service
            if (selectedListBeans != null && service != null && service.exoPlayer != null && service.exoPlayer.playbackState.equals(
                    PlaybackState.STATE_PLAYING
                ) && service.isRadioOn
            ) {
                service.notificationManager.setData(selectedListBeans)
                service.notificationManager.startNotify(service.status,true)
            }
        }

        override fun onServiceDisconnected(arg0: ComponentName) {
        }
    }
}