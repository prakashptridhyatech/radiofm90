package com.shtibel.fm90.player;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.Drawable;
import android.os.Build;

import androidx.core.app.NotificationCompat;
import androidx.core.app.NotificationManagerCompat;

import com.shtibel.fm90.R;
import com.shtibel.fm90.activity.SplashActivity;
import com.shtibel.fm90.model.ListBeans;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Target;

public class MediaNotificationManager {

    public static final int NOTIFICATION_ID = 555;
    private final String PRIMARY_CHANNEL = "PRIMARY_CHANNEL_ID";
    private final String PRIMARY_CHANNEL_NAME = "PRIMARY";

    private RadioService service;

    private String strAppName, strLiveBroadcast;

    private Resources resources;

    private NotificationManagerCompat notificationManager;
    private ListBeans listBeans;
    private String imgUrl = "";
    private boolean isForeground = true;

    public void setData(ListBeans listBeans) {
        this.listBeans = listBeans;
        strAppName = listBeans != null ? (listBeans.getProgram_name().replace("&quot;", "\"")) : resources.getString(R.string.app_name);
        strLiveBroadcast = listBeans != null ? (listBeans.getBroadcaster_name().replace("&quot;", "\"")) : resources.getString(R.string.live_broadcast);
        imgUrl = listBeans != null ? listBeans.getBroadcaster_img() : "";
    }

    public MediaNotificationManager(RadioService service) {
        this.service = service;
        this.resources = service.getResources();
        strAppName = listBeans != null ? (listBeans.getProgram_name().replace("&quot;", "\"")) : resources.getString(R.string.app_name);
        strLiveBroadcast = listBeans != null ? (listBeans.getBroadcaster_name().replace("&quot;", "\"")) : resources.getString(R.string.live_broadcast);
        imgUrl = listBeans != null ? listBeans.getBroadcaster_img() : "";
        notificationManager = NotificationManagerCompat.from(service);
    }

    /*public void startNotify(String playbackStatus) {
        new AsyncTask<Void, Void, Bitmap>() {
            @Override
            protected Bitmap doInBackground(Void... voids) {
                InputStream in;
                try {

                    URL url = new URL(imgUrl);
                    HttpURLConnection connection = (HttpURLConnection) url.openConnection();
                    connection.setDoInput(true);
                    connection.connect();
                    in = connection.getInputStream();
                    return BitmapFactory.decodeStream(in);
                } catch (MalformedURLException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();

                }
                return null;
            }

            @Override
            protected void onPostExecute(Bitmap bitmap) {
                super.onPostExecute(bitmap);
                int icon = R.drawable.ic_pause_white;
                Intent playbackAction = new Intent(service, RadioService.class);
                playbackAction.setAction(RadioService.ACTION_PAUSE);
                PendingIntent action = PendingIntent.getService(service, 1, playbackAction, 0);

                if (playbackStatus.equals(PlaybackStatus.PAUSED)) {

                    icon = R.drawable.ic_play_white;
                    playbackAction.setAction(RadioService.ACTION_PLAY);
                    action = PendingIntent.getService(service, 2, playbackAction, 0);

                }

                Intent stopIntent = new Intent(service, RadioService.class);
                stopIntent.setAction(RadioService.ACTION_STOP);
                PendingIntent stopAction = PendingIntent.getService(service, 3, stopIntent, 0);

                Intent intent = new Intent(service, SplashActivity.class);
                intent.setAction(Intent.ACTION_MAIN);
                intent.addCategory(Intent.CATEGORY_LAUNCHER);
                PendingIntent pendingIntent = PendingIntent.getActivity(service, 0, intent, 0);

                notificationManager.cancel(NOTIFICATION_ID);

                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                    NotificationManager manager = (NotificationManager) service.getSystemService(Context.NOTIFICATION_SERVICE);
                    NotificationChannel channel = new NotificationChannel(PRIMARY_CHANNEL, PRIMARY_CHANNEL_NAME, NotificationManager.IMPORTANCE_LOW);
                    channel.setLockscreenVisibility(Notification.VISIBILITY_PRIVATE);
                    manager.createNotificationChannel(channel);
                }

                NotificationCompat.Builder builder = new NotificationCompat.Builder(service, PRIMARY_CHANNEL)
                        .setAutoCancel(false)
                        .setContentTitle(strAppName)
                        .setContentText(strLiveBroadcast)
                        .setLargeIcon(bitmap == null ? BitmapFactory.decodeResource(resources, R.drawable.splash_90fm_logo) : bitmap)
                        .setContentIntent(pendingIntent)
                        .setVisibility(NotificationCompat.VISIBILITY_PUBLIC)
                        .setSmallIcon(R.drawable.ic_radio_black_24dp)
                        .addAction(icon, "pause", action)
                        .addAction(R.drawable.ic_stop_white, "stop", stopAction)
                        .setPriority(NotificationCompat.PRIORITY_HIGH)
                        .setWhen(System.currentTimeMillis())
                        .setStyle(new androidx.media.app.NotificationCompat.MediaStyle()
                                .setMediaSession(service.getMediaSession().getSessionToken())
                                .setShowActionsInCompactView(0, 1)
                                .setShowCancelButton(true)
                                .setCancelButtonIntent(stopAction));

//        if (isForeground)
                service.startForeground(NOTIFICATION_ID, builder.build());
//        else
//            notificationManager.notify(NOTIFICATION_ID, builder.build());

//        Bitmap largeIcon = BitmapFactory.decodeResource(resources, R.drawable.demo_image);
            }
        }.execute();

    }
*/
    public void startNotify(String playbackStatus, boolean isRadioOn) {
        if (!imgUrl.isEmpty())
            Picasso
                    .get()
                    .load(imgUrl)
                    .into(new Target() {
                              @Override
                              public void onBitmapLoaded(Bitmap bitmap, Picasso.LoadedFrom from) {
                                  if (isRadioOn)
                                      setNotification(playbackStatus, bitmap);
                                  else
                                      setNotificationArchive(playbackStatus, bitmap);
                              }

                              @Override
                              public void onBitmapFailed(Exception e, Drawable errorDrawable) {
                                  if (isRadioOn)
                                      setNotification(playbackStatus, null);
                                  else
                                      setNotificationArchive(playbackStatus, null);
                              }

                              @Override
                              public void onPrepareLoad(Drawable placeHolderDrawable) {
                              }
                          }
                    );
        else if (isRadioOn)
            setNotification(playbackStatus, null);
        else
            setNotificationArchive(playbackStatus, null);
    }
    /*public void startNotify(String playbackStatus, boolean isRadioOn) {
        Glide.with(ApplicationClass.context)
                .asBitmap()
                .load(imgUrl)
                .into(new CustomTarget<Bitmap>() {
                    @Override
                    public void onResourceReady(@NonNull Bitmap resource, @Nullable Transition<? super Bitmap> transition) {
                        if (isRadioOn)
                            setNotification(playbackStatus, resource);
                        else
                            setNotificationArchive(playbackStatus, resource);
                    }

                    @Override
                    public void onLoadCleared(@Nullable Drawable placeholder) {

                    }

                    @Override
                    public void onLoadFailed(@Nullable Drawable errorDrawable) {
                        super.onLoadFailed(errorDrawable);
                        if (isRadioOn)
                            setNotification(playbackStatus, null);
                        else
                            setNotificationArchive(playbackStatus, null);
                    }
                });
    }*/

    private void setNotification(String playbackStatus, Bitmap bitmap) {
        int icon = R.drawable.ic_pause_white;
        Intent playbackAction = new Intent(service, RadioService.class);
        playbackAction.setAction(RadioService.ACTION_PAUSE);
        PendingIntent action = PendingIntent.getService(service, 1, playbackAction, 0);

        if (playbackStatus.equals(PlaybackStatus.PAUSED)) {

            icon = R.drawable.ic_play_button;
            playbackAction.setAction(RadioService.ACTION_PLAY);
            action = PendingIntent.getService(service, 2, playbackAction, 0);

        }

        Intent stopIntent = new Intent(service, RadioService.class);
        stopIntent.setAction(RadioService.ACTION_STOP);
        PendingIntent stopAction = PendingIntent.getService(service, 3, stopIntent, 0);

        Intent intent = new Intent(service, SplashActivity.class);
        intent.setAction(Intent.ACTION_MAIN);
        intent.addCategory(Intent.CATEGORY_LAUNCHER);
        PendingIntent pendingIntent = PendingIntent.getActivity(service, 0, intent, 0);

        notificationManager.cancel(NOTIFICATION_ID);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            NotificationManager manager = (NotificationManager) service.getSystemService(Context.NOTIFICATION_SERVICE);
            NotificationChannel channel = new NotificationChannel(PRIMARY_CHANNEL, PRIMARY_CHANNEL_NAME, NotificationManager.IMPORTANCE_LOW);
            channel.setLockscreenVisibility(Notification.VISIBILITY_PRIVATE);
            manager.createNotificationChannel(channel);
        }

        NotificationCompat.Builder builder = new NotificationCompat.Builder(service, PRIMARY_CHANNEL)
                .setAutoCancel(false)
                .setContentTitle(strAppName)
                .setContentText(strLiveBroadcast)
                .setLargeIcon(bitmap == null ? BitmapFactory.decodeResource(resources, R.drawable.splash_90fm_logo) : bitmap)
                .setContentIntent(pendingIntent)
                .setVisibility(NotificationCompat.VISIBILITY_PUBLIC)
                .setSmallIcon(R.drawable.ic_radio_black_24dp)
                .addAction(icon, "pause", action)
                .addAction(R.drawable.ic_stop_white, "stop", stopAction)
                .setPriority(NotificationCompat.PRIORITY_HIGH)
                .setWhen(System.currentTimeMillis())
                .setStyle(new androidx.media.app.NotificationCompat.MediaStyle()
                        .setMediaSession(service.getMediaSession().getSessionToken())
                        .setShowActionsInCompactView(0, 1)
                        .setShowCancelButton(true)
                        .setCancelButtonIntent(stopAction));

        service.startForeground(NOTIFICATION_ID, builder.build());
    }

    private void setNotificationArchive(String playbackStatus, Bitmap bitmap) {
        int icon = R.drawable.ic_pause_white;
        Intent playbackAction = new Intent(service, RadioService.class);
        playbackAction.setAction(RadioService.ACTION_PAUSE);
        PendingIntent action = PendingIntent.getService(service, 1, playbackAction, 0);

        if (playbackStatus.equals(PlaybackStatus.PAUSED)) {

            icon = R.drawable.ic_play_button;
            playbackAction.setAction(RadioService.ACTION_PLAY);
            action = PendingIntent.getService(service, 2, playbackAction, 0);

        }

        Intent stopIntent = new Intent(service, RadioService.class);
        stopIntent.setAction(RadioService.ACTION_STOP);
        PendingIntent stopAction = PendingIntent.getService(service, 3, stopIntent, 0);

        Intent nextIntent = new Intent(service, RadioService.class);
        nextIntent.setAction(RadioService.ACTION_NEXT);
        PendingIntent nextAction = PendingIntent.getService(service, 4, nextIntent, 0);

        Intent previousIntent = new Intent(service, RadioService.class);
        previousIntent.setAction(RadioService.ACTION_PREVIOUS);
        PendingIntent previousAction = PendingIntent.getService(service, 4, previousIntent, 0);

        Intent intent = new Intent(service, SplashActivity.class);
        intent.setAction(Intent.ACTION_MAIN);
        intent.addCategory(Intent.CATEGORY_LAUNCHER);
        PendingIntent pendingIntent = PendingIntent.getActivity(service, 0, intent, 0);

        notificationManager.cancel(NOTIFICATION_ID);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            NotificationManager manager = (NotificationManager) service.getSystemService(Context.NOTIFICATION_SERVICE);
            NotificationChannel channel = new NotificationChannel(PRIMARY_CHANNEL, PRIMARY_CHANNEL_NAME, NotificationManager.IMPORTANCE_LOW);
            channel.setLockscreenVisibility(Notification.VISIBILITY_PRIVATE);
            manager.createNotificationChannel(channel);
        }

        NotificationCompat.Builder builder = new NotificationCompat.Builder(service, PRIMARY_CHANNEL)
                .setAutoCancel(false)
                .setContentTitle(strAppName)
                .setContentText(strLiveBroadcast)
                .setLargeIcon(bitmap == null ? BitmapFactory.decodeResource(resources, R.drawable.splash_90fm_logo) : bitmap)
                .setContentIntent(pendingIntent)
                .setVisibility(NotificationCompat.VISIBILITY_PUBLIC)
                .setSmallIcon(R.drawable.ic_radio_black_24dp)
                .addAction(R.drawable.ic_skip_previous_black_24dp, "next", nextAction)
                .addAction(icon, "pause", action)
                .addAction(R.drawable.ic_stop_white, "stop", stopAction)
                .addAction(R.drawable.ic_skip_next_black_24dp, "previous", previousAction)
                .setPriority(NotificationCompat.PRIORITY_HIGH)
                .setWhen(System.currentTimeMillis())
                .setStyle(new androidx.media.app.NotificationCompat.MediaStyle()
                        .setMediaSession(service.getMediaSession().getSessionToken())
                        .setShowActionsInCompactView(0, 1)
                        .setShowCancelButton(true)
                        .setCancelButtonIntent(stopAction));

        service.startForeground(NOTIFICATION_ID, builder.build());
    }

    void cancelNotify() {
        service.stopForeground(true);
    }

    public void setForeground(boolean isForeground) {
        this.isForeground = isForeground;
    }
}
