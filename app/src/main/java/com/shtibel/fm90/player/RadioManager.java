package com.shtibel.fm90.player;

import android.annotation.SuppressLint;
import android.app.ActivityManager;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.IBinder;

import com.shtibel.fm90.model.ListBeans;
import com.shtibel.fm90.model.ProgramArchiveData;

import org.greenrobot.eventbus.EventBus;

import java.util.List;

public class RadioManager {

    public static final String CURRENT_PROGRAM = "currentProgram";
    @SuppressLint("StaticFieldLeak")
    private static RadioManager instance = null;

    public static RadioService service;

    private Context context;
    private ListBeans selectedListBeans;
    private boolean serviceBound;

    private RadioManager(Context context, ListBeans selectedListBeans) {
        this.context = context;
        this.selectedListBeans = selectedListBeans;
        serviceBound = false;
    }

    private RadioManager(Context context) {
        this.context = context;
        serviceBound = false;
    }

    public static RadioManager with(Context context, ListBeans selectedListBeans) {
        if (instance == null)
            instance = new RadioManager(context, selectedListBeans);

        return instance;
    }

    public static RadioManager with(Context context) {
        if (instance == null)
            instance = new RadioManager(context);

        return instance;
    }

    public static RadioService getService() {
        return service;
    }

    public void playOrPause(String streamUrl, ListBeans listBeans, boolean isRadioOn, int position, List<ProgramArchiveData> programArchiveData) {
        service.setData(listBeans, isRadioOn, position, programArchiveData);
        service.notificationManager.setData(listBeans);
        service.playOrPause(streamUrl);
    }

    public boolean isPlaying() {

        return service.isPlaying();
    }

    public void bind() {

        Intent intent = new Intent(context, RadioService.class);
        intent.putExtra(CURRENT_PROGRAM, selectedListBeans);
        if (!isMyServiceRunning(RadioService.class))
            context.startService(intent);
        context.bindService(intent, serviceConnection, Context.BIND_AUTO_CREATE);

        if (service != null)
            EventBus.getDefault().post(service.getStatus());
    }

    public void unbind() {
        if (serviceBound && serviceConnection != null) {
            try {
                context.unbindService(serviceConnection);
            } catch (Exception e) {
            }
        }
    }

    private boolean isMyServiceRunning(Class<?> serviceClass) {
        ActivityManager manager = (ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE);
        for (ActivityManager.RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE)) {
            if (serviceClass.getName().equals(service.service.getClassName())) {
                return true;
            }
        }
        return false;
    }

    private ServiceConnection serviceConnection = new ServiceConnection() {

        @Override
        public void onServiceConnected(ComponentName arg0, IBinder binder) {

            service = ((RadioService.LocalBinder) binder).getService();
            serviceBound = true;
        }

        @Override
        public void onServiceDisconnected(ComponentName arg0) {

            serviceBound = false;
        }
    };
}
