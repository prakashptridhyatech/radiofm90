package com.shtibel.fm90

import android.annotation.SuppressLint
import android.app.Application
import android.content.Context
import android.os.StrictMode
import android.util.Log
import com.onesignal.OSSubscriptionObserver
import com.onesignal.OSSubscriptionStateChanges
import com.onesignal.OneSignal
import com.shtibel.fm90.webservice.APIs
import com.shtibel.fm90.webservice.ApiInterface
import com.shtibel.fm90.webservice.JSONCallback
import com.shtibel.fm90.webservice.Retrofit
import okhttp3.*
import okhttp3.MediaType.Companion.toMediaTypeOrNull
import org.json.JSONObject
import retrofit2.Call
import retrofit2.adapter.rxjava.RxJavaCallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import java.io.IOException
import java.util.concurrent.TimeUnit


@SuppressLint("Registered")
class ApplicationClass : Application(), OSSubscriptionObserver {

    override fun onCreate() {
        super.onCreate()
        context = this

        /*Fabric.with(this, Crashlytics())
        FirebaseApp.initializeApp(this)
        FirebaseApp.initializeApp(applicationContext)*/
        val builder = StrictMode.VmPolicy.Builder()
        StrictMode.setVmPolicy(builder.build())

        // OneSignal Initialization
        OneSignal.startInit(this)
            .inFocusDisplaying(OneSignal.OSInFocusDisplayOption.Notification)
            .unsubscribeWhenNotificationsAreDisabled(true)
            .init();
//        OneSignal.addSubscriptionObserver(this);

    }

   public companion object {
       lateinit var context: ApplicationClass
       val REFRESH_WATCH_LIST = "Refresh_Watch_List"
        val REFRESH_NOTIFICATION_COUNT = "NOtification_count"
        val REFRESH_PRODUCT_LIST = "Refresh_Product_List"
        val ACTION_MONTH_LIST = "action_month_list"

        val appContext: Context?
            get() = context
    }

    override fun onOSSubscriptionChanged(stateChanges: OSSubscriptionStateChanges?) {
//        Log.e("onOSSubscriptionChanged", stateChanges!!.to.userId)
        makeCall(stateChanges!!.to.userId)
        /*     Log.e("userid", "" + stateChanges!!.to.userId)
        val userID = OneSignal.getPermissionSubscriptionState().subscriptionStatus.userId
        Log.e("userID", userID)*/
    }

    private fun makeCall(userId: String) {
        val call: Call<ResponseBody>

        val client = OkHttpClient.Builder()
            .connectTimeout(60, TimeUnit.SECONDS)
            .readTimeout(60, TimeUnit.SECONDS)
            .writeTimeout(60, TimeUnit.SECONDS)
            .addInterceptor(Retrofit.provideHttpLoggingInterceptor())
            .addInterceptor(object : Interceptor {
                @Throws(IOException::class)
                public override fun intercept(chain: Interceptor.Chain): Response {
                    val original = chain.request()
                    val request: Request
                    request =
                        chain.request().newBuilder().method(original.method, original.body)
                            .build()
                    return chain.proceed(request)
                }
            }).build()

        val APIInterface = retrofit2.Retrofit.Builder()
            .baseUrl(APIs.BASE_URL)
            .client(client).addCallAdapterFactory(RxJavaCallAdapterFactory.create())
            .addConverterFactory(GsonConverterFactory.create())
            .build().create<ApiInterface>(
                ApiInterface::class.java
            )

        val paramsJson = JSONObject()
        paramsJson.put("onesignal_id", userId)
        paramsJson.put("device_type", "Android")
        paramsJson.put("app_version", "1.0")
        val bodyRequest =
            RequestBody.create("application/json".toMediaTypeOrNull(), paramsJson.toString())
        call = APIInterface.callPostMethod(APIs.BASE + "?method=setUserData", bodyRequest)
        call.enqueue(object :
            JSONCallback(context!!, null) {
            override fun onFailed(statusCode: Int, message: String) {
                Log.e("onFailed", "message" + message)
            }

            override fun onSuccess(statusCode: Int, jsonObject: JSONObject) {
                Log.e("onSuccess", jsonObject.toString())
            }
        })
    }
}