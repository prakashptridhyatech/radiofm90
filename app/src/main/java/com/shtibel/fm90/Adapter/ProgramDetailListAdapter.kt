package com.shtibel.fm90.Adapter

import android.text.TextUtils
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.google.android.exoplayer2.Player
import com.google.android.exoplayer2.SimpleExoPlayer
import com.shtibel.fm90.R
import com.shtibel.fm90.databinding.ItemProgramDetailListBinding
import com.shtibel.fm90.interfaces.OnClubItemClicked
import com.shtibel.fm90.model.ProgramArchiveData
import com.shtibel.fm90.player.PlaybackStatus
import com.shtibel.fm90.player.RadioManager
import java.text.SimpleDateFormat


class ProgramDetailListAdapter(
    private var mProgramArchiveList: List<ProgramArchiveData>,
    private var listner: OnClubItemClicked<Int>,
    private var selectedPosition: Int,
    private var status: String
) : RecyclerView.Adapter<ProgramDetailListAdapter.ViewHolder>() {

    private lateinit var mBinding: ItemProgramDetailListBinding
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        mBinding = DataBindingUtil.inflate(
            LayoutInflater.from(parent.context),
            R.layout.item_program_detail_list,
            parent,
            false
        )
        return ViewHolder(mBinding)

    }

    override fun getItemCount(): Int = mProgramArchiveList.size


    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        if (selectedPosition == position) {
            if (!TextUtils.isEmpty(status) && status == PlaybackStatus.PLAYING || status == PlaybackStatus.LOADING) {
                holder.mBinder.imgPlay.setImageResource(R.drawable.ic_vector_pause_detail)
            } else if (isPlaying(RadioManager.getService().exoPlayer)) {
                holder.mBinder.imgPlay.setImageResource(R.drawable.ic_vector_pause_detail)
            } else {
                holder.mBinder.imgPlay.setImageResource(R.drawable.ic_vector_play_detail)
            }
        } else {
            holder.mBinder.imgPlay.setImageResource(R.drawable.ic_vector_play_detail)
        }

        holder.bind(mProgramArchiveList[position])
        holder.mBinder.imgPlay.setOnClickListener {
            listner.onClubItemClicked(position)
        }
    }

    fun isPlaying(exoplayer: SimpleExoPlayer): Boolean {
        return exoplayer.playbackState == Player.STATE_READY && exoplayer.playWhenReady;
    }

    class ViewHolder(var mBinder: ItemProgramDetailListBinding) :
        RecyclerView.ViewHolder(mBinder.root) {

        fun bind(mProgramArchiveData: ProgramArchiveData) {
            mBinder.txtProgramName.text = (mProgramArchiveData.program_name.replace("&quot;", "\""))
            if (mProgramArchiveData.program_date != null && mProgramArchiveData.program_date.isNotEmpty()) {
                val sdf = SimpleDateFormat("yyyy-MM-dd")
                val sdf1 = SimpleDateFormat("dd/MM/yyyy")
                val date = mProgramArchiveData.program_date.replace("&quot;", "\"")
                val d = sdf.parse(date)
                mBinder.txtProgramDate.text = sdf1.format(d!!)
            }
        }
    }


}