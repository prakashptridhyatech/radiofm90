package com.shtibel.fm90.Adapter

import android.annotation.SuppressLint
import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.shtibel.fm90.R
import com.shtibel.fm90.databinding.ItemScheduleListBinding
import com.shtibel.fm90.interfaces.OnClubItemClicked
import com.shtibel.fm90.model.ListBeans
import com.squareup.picasso.Picasso


class ScheduleListAdapter(
    val context: Context,
    var mScheduleList: ArrayList<ListBeans>,
    var listener: OnClubItemClicked<ListBeans>
) : RecyclerView.Adapter<ScheduleListAdapter.ViewHolder>() {

    private lateinit var mBinding: ItemScheduleListBinding
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        mBinding = DataBindingUtil.inflate(
            LayoutInflater.from(parent.context),
            R.layout.item_schedule_list,
            parent,
            false
        )
        return ViewHolder(mBinding)

    }

    override fun getItemCount(): Int = mScheduleList.size


    override fun onBindViewHolder(holder: ViewHolder, position: Int) {

        holder.bind(context, mScheduleList[position])
        holder.itemView.setOnClickListener {
            listener.onClubItemClicked(mScheduleList[position])
//            context.startActivity(Intent(context, ProgramDetailActivity::class.java))
        }
    }


    class ViewHolder(var mBinder: ItemScheduleListBinding) :
        RecyclerView.ViewHolder(mBinder.root) {

        @SuppressLint("UnsafeExperimentalUsageError")
        fun bind(context: Context, mSchedules: ListBeans) {
            mBinder.txtProgramName.text = (mSchedules.program_name).replace("&quot;", "\"")
            mBinder.txtProgramRJ.text = (mSchedules.broadcaster_name).replace("&quot;", "\"")
            mBinder.txtProgramTime.text =
                (mSchedules.program_start_time!!.replace(
                    ":00:00", ":00"
                ) + "-").plus(mSchedules.program_end_time.replace(":00:00", ":00"))
            Picasso.get().load(mSchedules.broadcaster_img)
                .into(mBinder.imgProgram)
        }
    }

}