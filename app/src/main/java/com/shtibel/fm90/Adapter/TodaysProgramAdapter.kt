package com.shtibel.fm90.Adapter

import android.content.Context
import android.text.TextUtils
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.shtibel.fm90.R
import com.shtibel.fm90.databinding.ItemTodaysProgramBinding
import com.shtibel.fm90.interfaces.OnClubItemClicked
import com.shtibel.fm90.model.ListBeans
import com.squareup.picasso.Picasso
import java.util.*


class TodaysProgramAdapter(
    val context: Context,
    var mTodaysProgramList: ArrayList<ListBeans>,
    var listner: OnClubItemClicked<ListBeans>
) : RecyclerView.Adapter<TodaysProgramAdapter.ViewHolder>() {

    private lateinit var mBinding: ItemTodaysProgramBinding
    private var selectedPosition = -1
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        mBinding = DataBindingUtil.inflate(
            LayoutInflater.from(parent.context),
            R.layout.item_todays_program,
            parent,
            false
        )
        return ViewHolder(mBinding)

    }

    override fun getItemCount(): Int = mTodaysProgramList.size


    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        if (position == selectedPosition) {
            holder.mBinder.layoutCurrent.visibility = View.VISIBLE
//            listner.onClubItemClicked(mTodaysProgramList[position])
        } else
            holder.mBinder.layoutCurrent.visibility = View.GONE

        holder.bind(context, mTodaysProgramList[position])
        holder.itemView.setOnClickListener {
            listner.onClubItemClicked(mTodaysProgramList[position])
        }
    }

    fun setSelectedPosition(i: Int) {
        this.selectedPosition = i;
    }


    class ViewHolder(var mBinder: ItemTodaysProgramBinding) :
        RecyclerView.ViewHolder(mBinder.root) {

        fun bind(
            context: Context,
            mTodaysProgramList: ListBeans
        ) {

            mBinder.txtProgramName.text = (mTodaysProgramList.program_name.replace("&quot;", "\""))
            mBinder.txtProgramRJ.text = (mTodaysProgramList.broadcaster_name.replace("&quot;", "\""))
            mBinder.txtProgramTime.text =
                (mTodaysProgramList.program_start_time!!.replace(":00:00", ":00"))
            if (!TextUtils.isEmpty(mTodaysProgramList.broadcaster_img))
                Picasso.get().load(mTodaysProgramList.broadcaster_img)
                    .into(mBinder.imageView)
            else
                mBinder.imageView.setImageResource(R.drawable.splash_90fm_logo)
        }
    }
}