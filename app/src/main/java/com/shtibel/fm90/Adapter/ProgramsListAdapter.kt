package com.shtibel.fm90.Adapter

import android.annotation.SuppressLint
import android.content.Context
import android.text.TextUtils
import android.view.LayoutInflater
import android.view.View.GONE
import android.view.View.VISIBLE
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.shtibel.fm90.R
import com.shtibel.fm90.databinding.ItemProgramsListBinding
import com.shtibel.fm90.interfaces.OnClubItemClicked
import com.shtibel.fm90.model.ListBeans
import com.squareup.picasso.Picasso


class ProgramsListAdapter(
    val context: Context,
    var mProgramList: ArrayList<ListBeans>,
    var listner: OnClubItemClicked<ListBeans>,
    var selectedId: String
) : RecyclerView.Adapter<ProgramsListAdapter.ViewHolder>() {

    private lateinit var mBinding: ItemProgramsListBinding
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        mBinding = DataBindingUtil.inflate(
            LayoutInflater.from(parent.context),
            R.layout.item_programs_list,
            parent,
            false
        )
        return ViewHolder(mBinding)

    }

    override fun getItemCount(): Int = mProgramList.size


    override fun onBindViewHolder(holder: ViewHolder, position: Int) {

        if (!TextUtils.isEmpty(selectedId) && selectedId == mProgramList[position].id) {
            holder.mBinder.equalizerItem.visibility = VISIBLE
            holder.mBinder.equalizerItem.animateBars()
        } else {
            holder.mBinder.equalizerItem.visibility = GONE
        }
        holder.bind(context, mProgramList[position])
        holder.itemView.setOnClickListener {
            listner.onClubItemClicked(mProgramList[position])
        }
    }

    class ViewHolder(var mBinder: ItemProgramsListBinding) :
        RecyclerView.ViewHolder(mBinder.root) {

        @SuppressLint("UnsafeExperimentalUsageError")
        fun bind(context: Context, mProgram: ListBeans) {
            if (!TextUtils.isEmpty(mProgram.broadcaster_img))
                mBinder.txtProgramName.text = mProgram.program_name!!.replace("&quot;", "\"")
            else
                mBinder.txtProgramName.text = ""
            if (!TextUtils.isEmpty(mProgram.broadcaster_img))
                mBinder.txtProgramRJ.text =
                    (mProgram.broadcaster_name!!.replace("&quot;", "\""))
            else
                mBinder.txtProgramRJ.text = ""

            if (!TextUtils.isEmpty(mProgram.broadcaster_img))
                Picasso.get().load(mProgram.broadcaster_img)
                    .into(mBinder.imgProgram)
            else
                mBinder.imgProgram.setImageResource(R.drawable.splash_90fm_logo)
        }
    }
}