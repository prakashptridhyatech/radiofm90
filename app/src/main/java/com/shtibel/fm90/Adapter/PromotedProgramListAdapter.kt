package com.shtibel.fm90.Adapter

import android.annotation.SuppressLint
import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.shape.CornerFamily
import com.shtibel.fm90.R
import com.shtibel.fm90.databinding.ItemPromotedProgramListBinding
import com.shtibel.fm90.interfaces.OnClubItemClicked
import com.shtibel.fm90.model.PromotedProgramsList
import com.squareup.picasso.Picasso


class PromotedProgramListAdapter(
    val context: Context,
    var mPromotedProgramsList: ArrayList<PromotedProgramsList>,
    var listner: OnClubItemClicked<PromotedProgramsList>
) : RecyclerView.Adapter<PromotedProgramListAdapter.ViewHolder>() {

    private lateinit var mBinding: ItemPromotedProgramListBinding
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        mBinding = DataBindingUtil.inflate<ItemPromotedProgramListBinding>(
            LayoutInflater.from(parent.context),
            R.layout.item_promoted_program_list,
            parent,
            false
        )
        return ViewHolder(mBinding)

    }

    override fun getItemCount(): Int = mPromotedProgramsList.size


    override fun onBindViewHolder(holder: ViewHolder, position: Int) {

        holder.bind(context, mPromotedProgramsList[position])
        holder.itemView.setOnClickListener {
            listner.onClubItemClicked(mPromotedProgramsList[position])
        }
    }


    class ViewHolder(var mBinder: ItemPromotedProgramListBinding) :
        RecyclerView.ViewHolder(mBinder.root) {

        @SuppressLint("UnsafeExperimentalUsageError")
        fun bind(
            context: Context,
            mPromotedProgramsList: PromotedProgramsList
        ) {
            val radius: Float = context.resources.getDimension(com.intuit.sdp.R.dimen._60sdp)
            val radius10: Float = context.resources.getDimension(com.intuit.sdp.R.dimen._20sdp)

            mBinder.imgProgram.setShapeAppearanceModel(
                mBinder.imgProgram.getShapeAppearanceModel()
                    .toBuilder()
                    .setBottomLeftCorner(CornerFamily.ROUNDED, radius)
                    .setTopLeftCorner(CornerFamily.ROUNDED, radius)
                    .setTopRightCorner(CornerFamily.ROUNDED, radius10)
                    .setBottomRightCorner(CornerFamily.ROUNDED, radius10)
                    .build()
            )

            mBinder.txtProgramName.text = mPromotedProgramsList.program_name.replace("&quot;", "\"")
            mBinder.txtProgramRJ.text =
                (mPromotedProgramsList.broadcaster_name.replace("&quot;", "\""))
            Picasso.get().load(mPromotedProgramsList.broadcaster_img)
                .into(mBinder.imgProgram)
        }
    }
}