package com.shtibel.fm90.util

import android.app.Activity
import android.content.Context
import android.content.Context.MODE_PRIVATE
import android.content.Intent
import android.content.SharedPreferences
import com.google.gson.Gson
import com.shtibel.fm90.MainActivity
import com.shtibel.fm90.R
import com.shtibel.fm90.model.BroadcastScheduleModel
import com.shtibel.fm90.model.GeneralDataModel

class SessionManager(private val mContext: Context) {
    private val pref: SharedPreferences

    val userDetail: GeneralDataModel?
        get() {
            val gson = Gson()
            val json = pref.getString(KEY_USER_MODEL, "")
            return gson.fromJson<GeneralDataModel>(json, GeneralDataModel::class.java)
        }
    val broadcastScheduleListModel: BroadcastScheduleModel?
        get() {
            val json = pref.getString(KEY_Broadcast_Schedule, "")
            if (json == null) return null
            else
                return Gson().fromJson<BroadcastScheduleModel>(
                    json,
                    BroadcastScheduleModel::class.java
                )
        }

    val isLoggedIn: Boolean
        get() = pref.getBoolean(IS_LOGIN, false)

    init {
        val PREF_NAME = mContext.resources.getString(R.string.app_name)
        pref = mContext.getSharedPreferences(
            PREF_NAME, MODE_PRIVATE
        )
    }

    fun storeUserDetail(generalDataModel: GeneralDataModel) {
        val gson = Gson()
        val json = gson.toJson(generalDataModel) // myObject - instance of UserModel

        val editor: SharedPreferences.Editor = getEditor(mContext)
        editor.putString(KEY_USER_MODEL, json)
        editor.commit()
    }

    fun storeBroadcastSchedule(broadcastSchedule: BroadcastScheduleModel) {
        val gson = Gson()
        val json = gson.toJson(broadcastSchedule) // myObject - instance of UserModel

        val editor: SharedPreferences.Editor = getEditor(mContext)
        editor.putString(KEY_Broadcast_Schedule, json)
        editor.commit()
    }

    @JvmOverloads
    fun getDataByKey(Key: String, DefaultValue: String = ""): String {
        val returnValue: String
        if (pref.contains(Key)) {
            returnValue = pref.getString(Key, DefaultValue)!!
        } else {
            returnValue = DefaultValue
        }
        return returnValue
    }

    fun getDataByKey(Key: String, DefaultValue: Boolean): Boolean? {
        return if (pref.contains(Key)) {
            pref.getBoolean(Key, DefaultValue)
        } else {
            DefaultValue
        }
    }

    fun getDataByKey(Key: String, DefaultValue: Int): Int {
        return if (pref.contains(Key)) {
            pref.getInt(Key, DefaultValue)
        } else {
            DefaultValue
        }
    }

    fun storeDataByKey(key: String, Value: Int) {
        val editor: SharedPreferences.Editor = getEditor(mContext)
        editor.putInt(key, Value)
        editor.commit()
    }

    fun storeDataByKey(key: String, Value: String) {
        val editor: SharedPreferences.Editor = getEditor(mContext)
        editor.putString(key, Value)
        editor.commit()
    }

    fun storeDataByKey(key: String, Value: Boolean) {
        val editor: SharedPreferences.Editor = getEditor(mContext)
        editor.putBoolean(key, Value)
        editor.commit()
    }

    fun clearDataByKey(key: String) {
        val editor: SharedPreferences.Editor = getEditor(mContext)
        editor.remove(key).commit()
    }

    // Reset Challenge
    fun logoutUser(context: Context) {
        storeDataByKey(IS_LOGIN, false)
        //        storeDataByKey(SessionManager.KEY_USER_MODEL, null);
        val editor = getEditor(context)
        editor.remove(KEY_USER_MODEL)
            .apply()

        // Redirect to Welcome Screen
        val intent = Intent(context, MainActivity::class.java)
        intent.putExtra("BlankIntent", "BlankIntent")
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP or Intent.FLAG_ACTIVITY_NEW_TASK)
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
        context.startActivity(intent)
        (context as Activity).finish()
    }

    companion object {

        val KEY_LANGUAGE = "language"
        val IS_LOGIN = "pref_is_login"
        val KEY_CURRENCY = "user_currency"
        private val KEY_USER_MODEL = "pref_user_model"
        private val KEY_Broadcast_Schedule = "BroadcastScheduleModel"
        private val DEVICE_TOKEN = "device_token"
        private val KEY_PREF = "KEY_PREF"

        // region Helper Methods
        private fun getEditor(context: Context): SharedPreferences.Editor {
            val preferences =
                getSharedPreferences(
                    context
                )
            return preferences.edit()
        }

        private fun getSharedPreferences(context: Context): SharedPreferences {
            val PREF_NAME = context.resources.getString(R.string.app_name)
            return context.getSharedPreferences(PREF_NAME, Context.MODE_PRIVATE)
        }
    }
}
