package com.shtibel.fm90.util;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.pm.PackageManager;
import android.os.Build;

import androidx.core.app.ActivityCompat;

public class PermissionUtils {

    public static final String RECORD_AUDIO = Manifest.permission.RECORD_AUDIO;
    public static final String WRITE_EXTERNAL_STORAGE = Manifest.permission.WRITE_EXTERNAL_STORAGE;
    public static final int PERMISSION_CODE = 100;
    public static final int PERMISSION_CODE_FILE = 200;


    public static void isCheck(Context mContext, String[] permission, int requestCode, permissionListener mListener) {
        if (hasPermissions(mContext, permission)) {
            if (mListener != null)
                mListener.onAllow(requestCode);
        } else {
            ActivityCompat.requestPermissions((Activity) mContext, permission, requestCode);
        }
    }

    public static void onRequestPermissionsResult(Context me, int requestCode, String[] permissions, int[] grantResults, permissionListener permissionListener) {
        boolean valid = true, showRationale = true, deny = true;

        for (int i = 0, len = grantResults.length; i < len; i++) {
            if (grantResults[i] == PackageManager.PERMISSION_GRANTED) {
                valid = valid && grantResults[i] == PackageManager.PERMISSION_GRANTED;
            } else {

                valid = valid && grantResults[i] == PackageManager.PERMISSION_GRANTED;
            }
            if (grantResults[i] == PackageManager.PERMISSION_DENIED) {
                boolean show = ActivityCompat.shouldShowRequestPermissionRationale((Activity) me, permissions[i]);
                if (show) {
                    // deny = deny && grantResults[i] == PackageManager.PERMISSION_DENIED;
                    showRationale = showRationale && !(grantResults[i] == PackageManager.PERMISSION_DENIED);
                } else {
                    // user also CHECKED "never ask again"
                    // you can either enable some fall back,
                    // disable features of your app
                    // or open another dialog explaining
                    // again the permission and directing to
                    // the app setting
                    //showRationale = show && grantResults[i] == PackageManager.PERMISSION_DENIED;

                    showRationale = showRationale && grantResults[i] == PackageManager.PERMISSION_DENIED;
                }
            }
        }

        if (valid)
            permissionListener.onAllow(requestCode);
        else if (showRationale)
            permissionListener.onDenyNeverAskAgain(requestCode);
        else
            permissionListener.onDeny(requestCode);
    }


    public interface permissionListener {
        void onAllow(int requestCode);

        void onDeny(int requestCode);

        void onDenyNeverAskAgain(int requestCode);
    }

    private static boolean hasPermissions(Context context, String... permissions) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && context != null && permissions != null) {
            for (String permission : permissions) {
                if (ActivityCompat.checkSelfPermission(context, permission) != PackageManager.PERMISSION_GRANTED) {
                    return false;
                }
            }
        }
        return true;
    }
}
