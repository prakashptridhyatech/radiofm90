package com.shtibel.fm90.util


import android.app.Activity
import android.app.AlertDialog
import android.content.Context
import android.content.Intent
import android.content.res.Configuration
import android.graphics.Bitmap
import android.graphics.Matrix
import android.graphics.drawable.Drawable
import android.net.ConnectivityManager
import android.net.NetworkInfo
import android.net.Uri
import android.os.Build
import android.preference.PreferenceManager
import android.provider.MediaStore
import android.provider.Settings
import android.util.Base64
import android.util.TypedValue
import android.view.View
import android.view.inputmethod.InputMethodManager
import android.widget.TextView
import androidx.appcompat.widget.AppCompatTextView
import androidx.vectordrawable.graphics.drawable.VectorDrawableCompat
import com.shtibel.fm90.base.BaseActivity
import com.shtibel.fm90.model.BroadcastScheduleModel
import com.shtibel.fm90.model.ListBeans
import com.shtibel.fm90.util.AppConstants.REQUEST_CODE
import java.io.ByteArrayOutputStream
import java.text.DecimalFormat
import java.text.DecimalFormatSymbols
import java.text.ParseException
import java.text.SimpleDateFormat
import java.util.*


object Utils {
    public fun isTimeInbetween(startTime: String, endTime: String): Boolean {
        try {
            val str = startTime.replace(":", "").toInt()
            val end = endTime.replace(":", "").toInt()
            val pattern = "HH:mm:ss"
            val simpleDateFormat = SimpleDateFormat(pattern)
            val currentString =
                simpleDateFormat.format(Calendar.getInstance().time).replace(":", "")
            val current = currentString.toInt()
            return str < current && end > current
        } catch (e: ParseException) {
            e.printStackTrace()
        }
        return false
    }

    public fun isTimeInbetween1(startTime: String, endTime: String): Long {
        val startcalendar = Calendar.getInstance()
        val endcalendar = Calendar.getInstance()
        val calendarTemp1 = Calendar.getInstance()
        val calendarTemp2 = Calendar.getInstance()
        val pattern = "HH:mm:ss"
        val simpleDateFormat = SimpleDateFormat(pattern)
        startcalendar.time = simpleDateFormat.parse(startTime)
        endcalendar.time = simpleDateFormat.parse(endTime)
        calendarTemp1.set(Calendar.HOUR_OF_DAY, startcalendar.get(Calendar.HOUR_OF_DAY))
        calendarTemp1.set(Calendar.MINUTE, startcalendar.get(Calendar.MINUTE))
        calendarTemp1.set(Calendar.SECOND, startcalendar.get(Calendar.SECOND))

        calendarTemp2.set(Calendar.HOUR_OF_DAY, endcalendar.get(Calendar.HOUR_OF_DAY))
        calendarTemp2.set(Calendar.MINUTE, endcalendar.get(Calendar.MINUTE))
        calendarTemp2.set(Calendar.SECOND, endcalendar.get(Calendar.SECOND))

        return calendarTemp2.time.time - calendarTemp1.time.time

    }
    public fun getData(broadcastScheduleListModel: BroadcastScheduleModel?): ArrayList<ListBeans> {
        when (Calendar.getInstance().get(Calendar.DAY_OF_WEEK)) {
            1 -> return broadcastScheduleListModel!!.first!!
            2 -> return broadcastScheduleListModel!!.second!!
            3 -> return broadcastScheduleListModel!!.three!!
            4 -> return broadcastScheduleListModel!!.four!!
            5 -> return broadcastScheduleListModel!!.five!!
            6 -> return broadcastScheduleListModel!!.six!!
            0 -> return broadcastScheduleListModel!!.seven!!
        }
        return broadcastScheduleListModel!!.first!!
    }
    fun setLocale(activity: Activity, lang: String) {
        val locale = Locale(lang)
        Locale.setDefault(locale)
        val config = Configuration()
        config.locale = locale
        activity.baseContext.resources.updateConfiguration(
            config,
            activity.baseContext.resources.displayMetrics
        )
    }

    fun convertPxToDp(value: Int, context: Context): Float {
        val px = TypedValue.applyDimension(
            TypedValue.COMPLEX_UNIT_PX,
            value.toFloat(),
            context.resources.getDisplayMetrics()
        )
        return px;
    }

    fun isConnectingToInternet(context: Context): Boolean {
        val connectivityManager =
            context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            val networks = connectivityManager.allNetworks
            var networkInfo: NetworkInfo
            for (mNetwork in networks) {
                networkInfo = connectivityManager.getNetworkInfo(mNetwork)
                if (networkInfo.state == NetworkInfo.State.CONNECTED) {
                    Logger.d(
                        "Network",
                        "NETWORK NAME: " + networkInfo.typeName
                    )
                    return true
                }
            }
        } else {
            if (connectivityManager != null) {
                val info = connectivityManager.allNetworkInfo
                if (info != null) {
                    for (anInfo in info) {
                        if (anInfo.state == NetworkInfo.State.CONNECTED) {
                            Logger.d(
                                "Network",
                                "NETWORK NAME: " + anInfo.typeName
                            )
                            return true
                        }
                    }
                }
            }
        }
        return false
    }

    fun showSettingsDialog(context: Activity?) {
        val builder: AlertDialog.Builder =
            AlertDialog.Builder(context!!)
        builder.setTitle("Need Permissions")
        builder.setMessage("This app needs permission to use this feature. You can grant them in app settings.")
        builder.setPositiveButton("GOTO SETTINGS") { dialog, p1 ->
            dialog!!.cancel()
            openSettings(context)
        }
        builder.setNegativeButton("Cancel") { dialog, p1 ->
            dialog!!.cancel()
        }
        builder.show()
    }

    private fun openSettings(context: Activity?) {
        // navigating user to app settings
        val intent = Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS)
        val uri: Uri = Uri.fromParts("package", context?.packageName, null)
        intent.data = uri
        context?.startActivityForResult(intent, REQUEST_CODE)
    }

    //Image Real Path from URI
    fun getRealPathFromURI(mContext: Context, contentURI: Uri): String? {
        val result: String?
        //        String[] filePathColumn = {MediaStore.Images.Media.DATA};
        val filePathColumn = arrayOf(MediaStore.Images.ImageColumns.DATA)
        val cursor = mContext.contentResolver.query(contentURI, filePathColumn, null, null, null)
        if (cursor == null) { // Source is Dropbox or other similar local file path
            result = contentURI.path
        } else {
            cursor.moveToFirst()
            val idx = cursor.getColumnIndex(filePathColumn[0])
            result = cursor.getString(idx)
            cursor.close()
        }
        return result
    }

    fun setCurrentDate(tvDay: AppCompatTextView, tvDate: AppCompatTextView) {
        val strDate = TimeStamp.currentDateElements
        tvDay.setText(strDate[0])
        tvDate.setText(strDate[1])
    }

    fun setVectorForPreLollipop(
        textView: TextView,
        resourceId: Int,
        activity: Context,
        position: Int
    ) {
        val icon: Drawable?
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.LOLLIPOP) {
            icon = VectorDrawableCompat.create(activity.resources, resourceId, activity.theme)
        } else {
            icon = activity.resources.getDrawable(resourceId, activity.theme)
        }
        when (position) {
            AppConstants.DRAWABLE_LEFT -> textView.setCompoundDrawablesWithIntrinsicBounds(
                icon,
                null,
                null,
                null
            )

            AppConstants.DRAWABLE_RIGHT -> textView.setCompoundDrawablesWithIntrinsicBounds(
                null,
                null,
                icon,
                null
            )

            AppConstants.DRAWABLE_TOP -> textView.setCompoundDrawablesWithIntrinsicBounds(
                null,
                icon,
                null,
                null
            )

            AppConstants.DRAWABLE_BOTTOM -> textView.setCompoundDrawablesWithIntrinsicBounds(
                null,
                null,
                null,
                icon
            )
        }
    }

    fun hideKeyboardFrom(context: Context, view: View) {
        val imm = context.getSystemService(Activity.INPUT_METHOD_SERVICE) as InputMethodManager
        imm.hideSoftInputFromWindow(view.windowToken, 0)
    }

    fun getActionBarHeight(mActivity: Activity): Int {
        if (mActivity.actionBar != null) {
            var actionBarHeight = (mActivity as BaseActivity).supportActionBar!!.height
            if (actionBarHeight != 0)
                return actionBarHeight
            val tv = TypedValue()
            if (mActivity.getTheme().resolveAttribute(android.R.attr.actionBarSize, tv, true))
                actionBarHeight = TypedValue.complexToDimensionPixelSize(
                    tv.data,
                    mActivity.getResources().displayMetrics
                )
            return actionBarHeight
        }
        return 0
    }

    fun redirectToRelatedPage(context: Context, directLink: String) {
        val httpIntent = Intent(Intent.ACTION_VIEW)
        httpIntent.data = Uri.parse(directLink)
        context.startActivity(httpIntent)
    }

    fun getFormattedAmountBehind(textView: AppCompatTextView, amount: String?) {
        var amount = amount
        val formatSymbols = DecimalFormatSymbols(Locale.US)
        val decimalFormat = DecimalFormat(
            getCurrencyFormat(
                textView.context
            ), formatSymbols
        )
        decimalFormat.applyPattern("#,###,###,###.00")

        //        Log.e(TAG, "amount------" + amount);
        if (amount != null && !amount.isEmpty()) {
            if (amount.contains(",")) {
                amount = amount.replace(",", "")
            }

            textView.text = getCurrencySymbol(textView.context)!! + decimalFormat.format(
                java.lang.Double.parseDouble(amount)
            )
        } else {
            textView.text = getCurrencySymbol(textView.context)!! + decimalFormat.format(0)
        }
    }

    fun setCurrencyFormat(languageId: String, context: Context) {
        val prefs = PreferenceManager.getDefaultSharedPreferences(context)
        prefs.edit().putString("currency_format", languageId).apply()
    }

    fun getCurrencyFormat(context: Context): String? {
        val prefs = PreferenceManager.getDefaultSharedPreferences(context)
        return prefs.getString("currency_format", "0.00")
    }

    fun getCurrencySymbol(context: Context): String? {
        val prefs = PreferenceManager.getDefaultSharedPreferences(context)
        return prefs.getString("currencySymbol", "$")
    }

    fun setCurrencySymbol(languageId: String, context: Context) {
        val prefs = PreferenceManager.getDefaultSharedPreferences(context)
        prefs.edit().putString("currencySymbol", languageId).apply()
    }

    fun getFormattedAmount(textView: AppCompatTextView, amount: String?) {
        val decimalFormat = DecimalFormat("#")

        if (amount != null && !amount.isEmpty())

            textView.text = decimalFormat.format(java.lang.Double.parseDouble(amount))
        else {
            textView.text = amount
        }
    }

    fun getFormattedAmountString(amount: String?): String? {
        val decimalFormat = DecimalFormat("#")

        return if (amount != null && !amount.isEmpty())
            decimalFormat.format(java.lang.Double.parseDouble(amount))
        else {
            amount
        }
    }

    fun getBitmap(photo: Bitmap): Bitmap {
        val bitmap: Bitmap
        if (photo.width > photo.height) {
            val matrix = Matrix()
            matrix.postRotate(90f)
            bitmap = Bitmap.createBitmap(photo, 0, 0, photo.width, photo.height, matrix, true)
        } else {
            bitmap = photo
        }
        return bitmap
    }

    fun bitmapToString(bitmap: Bitmap): String {
        val baos = ByteArrayOutputStream()
        bitmap.compress(Bitmap.CompressFormat.PNG, 100, baos)
        val b = baos.toByteArray()
        return Base64.encodeToString(b, Base64.DEFAULT)
    }


}