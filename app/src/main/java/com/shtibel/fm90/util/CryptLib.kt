package com.shtibel.fm90.util

import android.util.Base64

import java.io.UnsupportedEncodingException
import java.security.InvalidAlgorithmParameterException
import java.security.InvalidKeyException
import java.security.MessageDigest
import java.security.NoSuchAlgorithmException
import java.security.SecureRandom

import javax.crypto.BadPaddingException
import javax.crypto.Cipher
import javax.crypto.IllegalBlockSizeException
import javax.crypto.NoSuchPaddingException
import javax.crypto.spec.IvParameterSpec
import javax.crypto.spec.SecretKeySpec

class CryptLib @Throws(NoSuchAlgorithmException::class, NoSuchPaddingException::class)
private constructor() {
    private val KEY = "(!Jack%Sparrow_Black{Pearl})~20@3"
    private val IV = "ABCDEFGHIJKLMNOP"

    // cipher to be used for encryption and decryption
    private val _cx: Cipher

    // encryption key and initialization vector
    private val _key: ByteArray
    private val _iv: ByteArray

    /**
     * Encryption mode enumeration
     */
    private enum class EncryptMode {
        ENCRYPT, DECRYPT
    }

    init {
        // initialize the cipher with transformation AES/CBC/PKCS5Padding
        _cx = Cipher.getInstance("AES/CBC/PKCS5Padding")
        _key = ByteArray(32) //256 bit key space
        _iv = ByteArray(16) //128 bit IV
    }

    /**
     * @param inputText     Text to be encrypted or decrypted
     * @param encryptionKey Encryption key to used for encryption / decryption
     * @param mode          specify the mode encryption / decryption
     * @param initVector    Initialization vector
     * @return encrypted or decrypted bytes based on the mode
     * @throws UnsupportedEncodingException       Exception
     * @throws InvalidKeyException                Exception
     * @throws InvalidAlgorithmParameterException Exception
     * @throws IllegalBlockSizeException          Exception
     * @throws BadPaddingException                Exception
     */
    @Throws(UnsupportedEncodingException::class, InvalidKeyException::class, InvalidAlgorithmParameterException::class, IllegalBlockSizeException::class, BadPaddingException::class)
    private fun encryptDecrypt(inputText: String, encryptionKey: String,
                               mode: EncryptMode, initVector: String): ByteArray {

        val CHARSET = "UTF-8"
        var len = encryptionKey.toByteArray(charset(CHARSET)).size // length of the key	provided

        if (encryptionKey.toByteArray(charset(CHARSET)).size > _key.size)
            len = _key.size

        var ivlength = initVector.toByteArray(charset(CHARSET)).size

        if (initVector.toByteArray(charset(CHARSET)).size > _iv.size)
            ivlength = _iv.size

        System.arraycopy(encryptionKey.toByteArray(charset(CHARSET)), 0, _key, 0, len)
        System.arraycopy(initVector.toByteArray(charset(CHARSET)), 0, _iv, 0, ivlength)


        val keySpec = SecretKeySpec(_key, "AES") // Create a new SecretKeySpec for the specified key data and algorithm name.

        val ivSpec = IvParameterSpec(_iv) // Create a new IvParameterSpec instance with the bytes from the specified buffer iv used as initialization vector.

        // encryption
        if (mode == EncryptMode.ENCRYPT) {
            // Potentially insecure random numbers on Android 4.3 and older. Read for more info.
            // https://android-developers.blogspot.com/2013/08/some-securerandom-thoughts.html
            _cx.init(Cipher.ENCRYPT_MODE, keySpec, ivSpec)// Initialize this cipher instance
            return _cx.doFinal(inputText.toByteArray(charset(CHARSET))) // Finish multi-part transformation (encryption)
        } else {
            _cx.init(Cipher.DECRYPT_MODE, keySpec, ivSpec)// Initialize this cipher instance

            val decodedValue = Base64.decode(inputText.toByteArray(), Base64.DEFAULT)
            return _cx.doFinal(decodedValue) // Finish multi-part transformation (decryption)
        }
    }

    @Throws(Exception::class)
    internal fun encryptPlainText(plainText: String): String {
        return encryptPlainText(plainText, KEY, IV)
    }

    @Throws(Exception::class)
    private fun encryptPlainText(plainText: String, key: String, iv: String): String {
        val bytes = encryptDecrypt(plainText,
            SHA256(key, 32),
            EncryptMode.ENCRYPT, iv)
        val encryptedText = Base64.encodeToString(bytes, Base64.DEFAULT)
        Logger.e("Encrypted Text: ", "==> $encryptedText")

        return encryptedText
    }

    @Throws(Exception::class)
    fun decryptCipherText(cipherText: String): String {
        return decryptCipherText(cipherText, KEY, IV)
    }

    @Throws(Exception::class)
    private fun decryptCipherText(cipherText: String, key: String, iv: String): String {
        val bytes = encryptDecrypt(cipherText,
            SHA256(key, 32),
            EncryptMode.DECRYPT, iv)
        val decryptedText = String(bytes)
        Logger.e("Decrypted Text: ", "==> $decryptedText")
        return decryptedText
    }


    @Throws(Exception::class)
    fun encryptPlainTextWithRandomIV(plainText: String, key: String): String {
        val bytes = encryptDecrypt(generateRandomIV16() + plainText,
            SHA256(key, 32),
            EncryptMode.ENCRYPT, generateRandomIV16())
        return Base64.encodeToString(bytes, Base64.DEFAULT)
    }

    @Throws(Exception::class)
    private fun decryptCipherTextWithRandomIV(cipherText: String, key: String): String {
        val bytes = encryptDecrypt(cipherText,
            SHA256(key, 32),
            EncryptMode.DECRYPT, generateRandomIV16())
        val out = String(bytes)
        return out.substring(16, out.length)
    }


    /**
     * Generate IV with 16 bytes
     *
     * @return return random 16 IV bytes
     */
    private fun generateRandomIV16(): String {
        val ranGen = SecureRandom()
        val aesKey = ByteArray(16)
        ranGen.nextBytes(aesKey)
        val result = StringBuilder()
        for (b in aesKey) {
            result.append(String.format("%02x", b)) //convert to hex
        }
        return if (16 > result.toString().length) {
            result.toString()
        } else {
            result.toString().substring(0, 16)
        }
    }

    companion object {

        private var instance: CryptLib? = null

        @Throws(NoSuchPaddingException::class, NoSuchAlgorithmException::class)
        fun getInstance(): CryptLib {
            if (instance == null) {
                instance =
                    CryptLib()
            }
            return instance!!
        }

        /***
         * This function computes the SHA256 hash of input string
         * @param text input text whose SHA256 hash has to be computed
         * @param length length of the text to be returned
         * @return returns SHA256 hash of input text
         * @throws NoSuchAlgorithmException Exception
         * @throws UnsupportedEncodingException Exception
         */
        @Throws(NoSuchAlgorithmException::class, UnsupportedEncodingException::class)
        private fun SHA256(text: String, length: Int): String {

            val resultString: String
            val md = MessageDigest.getInstance("SHA-256")

            md.update(text.toByteArray(charset("UTF-8")))
            val digest = md.digest()

            val result = StringBuilder()
            for (b in digest) {
                result.append(String.format("%02x", b)) //convert to hex
            }

            if (length > result.toString().length) {
                resultString = result.toString()
            } else {
                resultString = result.toString().substring(0, length)
            }

            return resultString

        }
    }
}
