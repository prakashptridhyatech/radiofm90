package com.shtibel.fm90.util

object AppConstants {

    val BASE_DOMAIN = "http://php2.shaligraminfotech.com"//Local
    //    public static final String BASE_DOMAIN = "http://106.201.238.169:3001";   //Client
    //    public static final String BASE_DOMAIN = "http://www.google.com/";   //Server


    // Image Selection
    val CAMERA = 0x50
    val GALLERY = 0x51
    const val REQUEST_CODE: Int = 1111
    //Drawable Sides
    val DRAWABLE_LEFT = 0x29
    val DRAWABLE_RIGHT = 0x2A
    val DRAWABLE_TOP = 0x2B
    val DEVICE_TOKEN="Device_token"
    val DRAWABLE_BOTTOM = 0x2C
    val COMMON_BODY = "commonBody"
    val COMMON_HEAD = "commonHead"
    val productListItemClick = "productListItemClick"
    val watchListClick = "watchList"
    val favouriteItemClick = "favouriteItemClick"
    val ID = "id"

    //language
    val EN = "en"


}
