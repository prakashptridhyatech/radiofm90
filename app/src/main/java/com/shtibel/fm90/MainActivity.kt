package com.shtibel.fm90

import android.content.Intent
import android.view.MenuItem
import android.view.View
import androidx.appcompat.widget.PopupMenu
import androidx.navigation.findNavController
import androidx.navigation.ui.AppBarConfiguration
import androidx.navigation.ui.setupActionBarWithNavController
import androidx.navigation.ui.setupWithNavController
import com.shtibel.fm90.activity.AboutUsActivity
import com.shtibel.fm90.activity.ContactUsActivity
import com.shtibel.fm90.base.BaseActivity
import kotlinx.android.synthetic.main.activity_main.*


class MainActivity : BaseActivity() {


    override fun initViews() {
        setSupportActionBar(toolbar)

        val navController = findNavController(R.id.nav_host_fragment)
        // Passing each menu ID as a set of Ids because each
        // menu should be considered as top level destinations.
        val appBarConfiguration = AppBarConfiguration(
            setOf(
                R.id.navigation_home, R.id.navigation_schedule, R.id.navigation_program_list
            )
        )
        setupActionBarWithNavController(navController, appBarConfiguration)
        navView.setupWithNavController(navController)
        navView.menu.findItem(R.id.navigation_more).setOnMenuItemClickListener { item: MenuItem? ->
            showPopUp(findViewById(R.id.navigation_more))
//            showPopup(findViewById(R.id.navigation_more))
            return@setOnMenuItemClickListener true
        }
    }

    override fun setListeners() {

    }

    override fun getLayoutResId(): Int = R.layout.activity_main

    private fun showPopUp(view: View) {
        val popup =
            PopupMenu(this, view)
        val inflater = popup.menuInflater
        inflater.inflate(R.menu.menu_more, popup.menu)
        popup.setOnMenuItemClickListener { item: MenuItem? ->

            when (item!!.itemId) {
                R.id.action_about_us -> {
                    startActivity(Intent(this@MainActivity, AboutUsActivity::class.java))
                }
                R.id.action_contact_us -> {
                    startActivity(Intent(this@MainActivity, ContactUsActivity::class.java))

                }
                R.id.action_share_app -> {
                }
                R.id.action_rate_app -> {
                }
            }
            true
        }

        popup.show()
    }
}
