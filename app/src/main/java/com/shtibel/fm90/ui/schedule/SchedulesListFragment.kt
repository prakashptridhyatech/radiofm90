package com.shtibel.fm90.ui.schedule

import android.os.Bundle
import android.view.View
import androidx.core.os.bundleOf
import com.shtibel.fm90.Adapter.ScheduleListAdapter
import com.shtibel.fm90.R
import com.shtibel.fm90.activity.DashboardActivity
import com.shtibel.fm90.base.BaseFragment
import com.shtibel.fm90.interfaces.OnClubItemClicked
import com.shtibel.fm90.model.ListBeans
import com.shtibel.fm90.ui.programs.ProgramDetailFragment
import com.shtibel.fm90.util.AppConstants.ID
import com.shtibel.fm90.util.Utils
import kotlinx.android.synthetic.main.fragment_schedules_list.*

class SchedulesListFragment : BaseFragment(), OnClubItemClicked<ListBeans> {
    private lateinit var mProgramsListAdapter: ScheduleListAdapter
    private var mSchedulesList = ArrayList<ListBeans>()

    override fun setContentView(): Int = R.layout.fragment_schedules_list

    override fun initView(rootView: View?, savedInstanceState: Bundle?) {
        if (arguments!!.containsKey(ID)) {
            mSchedulesList =
                arguments!!.getParcelableArrayList<ListBeans>(ID) as ArrayList<ListBeans>
        }
        setAdapter()
    }

    override fun setListeners() {
        swipeRefresh.setOnRefreshListener {
            swipeRefresh.isRefreshing = false
            if (!Utils.isConnectingToInternet(mContext!!)) {
                showShortToast(getString(R.string.no_internet_connection))
            } else {
                (parentFragment as SchedulesFragment).getBroadcastScheduleList()
            }
        }
    }

    companion object {
        fun newInstance(text: java.util.ArrayList<ListBeans>): SchedulesListFragment {
            val fragment = SchedulesListFragment()
            val bundle = Bundle()
            bundle.putParcelableArrayList(ID, text)
            fragment.arguments = bundle
            return fragment
        }
    }

    private fun setAdapter() {
        mProgramsListAdapter = ScheduleListAdapter(
            this.mContext!!,
            mSchedulesList,
            this
        )
        rvPrograms.adapter = mProgramsListAdapter
    }

    override fun onClubItemClicked(data: ListBeans?) {
        val bundle = bundleOf(ID to data!!.id_site_page_program)
        val programDetailFragment = ProgramDetailFragment()
        programDetailFragment.arguments = bundle
        (activity as DashboardActivity).replaceFragment(programDetailFragment)
    }
}
