package com.shtibel.fm90.ui.programs

import android.os.Bundle
import android.view.View
import com.shtibel.fm90.R
import com.shtibel.fm90.base.BaseFragment

class BlankFragment : BaseFragment() {

    override fun setContentView(): Int = R.layout.activity_coming_soon

    override fun initView(rootView: View?, savedInstanceState: Bundle?) {
    }

    override fun setListeners() {
    }


}
