package com.shtibel.fm90.ui.home

import android.app.AlarmManager
import android.app.PendingIntent
import android.content.Context
import android.content.Intent
import android.media.session.PlaybackState
import android.net.Uri
import android.os.Bundle
import android.os.Handler
import android.provider.Settings
import android.view.View
import androidx.core.os.bundleOf
import com.google.android.exoplayer2.Player
import com.google.android.exoplayer2.SimpleExoPlayer
import com.google.android.material.appbar.AppBarLayout
import com.google.android.material.snackbar.Snackbar
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import com.shtibel.fm90.Adapter.PromotedProgramListAdapter
import com.shtibel.fm90.Adapter.TodaysProgramAdapter
import com.shtibel.fm90.R
import com.shtibel.fm90.activity.DashboardActivity
import com.shtibel.fm90.base.BaseFragment
import com.shtibel.fm90.interfaces.OnClubItemClicked
import com.shtibel.fm90.interfaces.OnSnackBarActionListener
import com.shtibel.fm90.model.BroadcastScheduleModel
import com.shtibel.fm90.model.GeneralDataModel
import com.shtibel.fm90.model.ListBeans
import com.shtibel.fm90.model.PromotedProgramsList
import com.shtibel.fm90.player.PlaybackStatus
import com.shtibel.fm90.player.RadioManager
import com.shtibel.fm90.receiver.NextProgramReceiver
import com.shtibel.fm90.ui.programs.ProgramDetailFragment
import com.shtibel.fm90.util.AppConstants
import com.shtibel.fm90.util.PermissionUtils
import com.shtibel.fm90.util.SessionManager
import com.shtibel.fm90.util.Utils
import com.shtibel.fm90.webservice.APIs
import com.shtibel.fm90.webservice.JSONCallback
import com.shtibel.fm90.webservice.Retrofit
import kotlinx.android.synthetic.main.fragment_home.view.*
import kotlinx.android.synthetic.main.fragment_home_test.*
import org.json.JSONObject
import java.text.SimpleDateFormat
import java.util.*
import java.util.Calendar.*
import kotlin.collections.HashMap


class HomeFragment : BaseFragment(), OnClubItemClicked<PromotedProgramsList> {

    private lateinit var mTodaysProgramAdapter: TodaysProgramAdapter
    private lateinit var mTodaysProgramList: ArrayList<ListBeans>
    private lateinit var mPromotedProgramsList: ArrayList<PromotedProgramsList>
    private lateinit var mPromotedProgramListAdapter: PromotedProgramListAdapter
    override fun setContentView(): Int = R.layout.fragment_home_test
    //    var radioManager: RadioManager? = null
    private lateinit var broadcastScheduleListModel: BroadcastScheduleModel
    private lateinit var session: SessionManager
    private var selectedListBeans: ListBeans? = null
    private var totalProgramTime: Long = 0
    val pattern = "HH:mm:ss"
    val simpleDateFormat = SimpleDateFormat(pattern)
    override fun initView(rootView: View?, savedInstanceState: Bundle?) {
        session = SessionManager(mContext!!)
        progress.isEnabled = false
        if (!Utils.isConnectingToInternet(mContext!!)) {
            showShortToast(getString(R.string.no_internet_connection))
            return
        } else {
            getBroadcastScheduleList()
            getPromotedProgramsList()
        }

        swipeRefresh.setOnRefreshListener {
            swipeRefresh.isRefreshing = false
            if (!Utils.isConnectingToInternet(mContext!!)) {
                showShortToast(getString(R.string.no_internet_connection))
            } else {
                getBroadcastScheduleList()
                getPromotedProgramsList()
            }
        }

    }

    private fun addOffsetListener() {
        app_bar.addOnOffsetChangedListener(object : AppBarLayout.OnOffsetChangedListener {
            override fun onOffsetChanged(appBarLayout: AppBarLayout?, verticalOffset: Int) {
                if (Math.abs(verticalOffset) == appBarLayout!!.totalScrollRange) {
                    // Collapsed
                    expandedImage.visibility = View.GONE

                } else if (verticalOffset == 0) {
                    expandedImage.visibility = View.VISIBLE
                    expandedImage.alpha = 01f
                    // Expanded
                } else {
                    // Somewhere in between
                    expandedImage.visibility = View.VISIBLE
                    expandedImage.alpha = 0.5f
                }
            }
        })
    }

    override fun setListeners() {
        imgPlay.setOnClickListener {
            playRadio()
        }
        addOffsetListener()
        requestPermissions()
    }

    private fun requestPermissions() {
        PermissionUtils.isCheck(
            activity, arrayOf(
                PermissionUtils.RECORD_AUDIO
            ),
            PermissionUtils.PERMISSION_CODE, object : PermissionUtils.permissionListener {
                override fun onDeny(requestCode: Int) {
                    showSnackBarSettings()
                }

                override fun onAllow(requestCode: Int) {
                }

                override fun onDenyNeverAskAgain(requestCode: Int) {
                    showSnackBarSettings()
                }

            }
        )

    }

    private fun setTodaysAdapter() {

        mTodaysProgramAdapter = TodaysProgramAdapter(
            activity!!,
            mTodaysProgramList
            , OnClubItemClicked {
                val bundle = bundleOf(AppConstants.ID to it.id_site_page_program)
                val programDetailFragment = ProgramDetailFragment()
                programDetailFragment.arguments = bundle
                (activity as DashboardActivity).replaceFragment(programDetailFragment)
            }
        )
        fragment_home.rvTodaysProgram.adapter = mTodaysProgramAdapter
        for (i in 0 until mTodaysProgramList.size) {
            if (Utils.isTimeInbetween(
                    mTodaysProgramList.get(i).program_start_time!!,
                    mTodaysProgramList.get(i).program_end_time
                )
            ) {
                selectedListBeans = mTodaysProgramList.get(i)
                mTodaysProgramAdapter.setSelectedPosition(i)
                fragment_home.rvTodaysProgram.layoutManager!!.scrollToPosition(i)
                totalProgramTime = Utils.isTimeInbetween1(
                    selectedListBeans!!.program_start_time!!,
                    selectedListBeans!!.program_end_time
                )
                val completed = Utils.isTimeInbetween1(
                    selectedListBeans!!.program_start_time!!,
                    simpleDateFormat.format(getInstance().time)
                )
                progress.progress = (completed / (totalProgramTime / 100)).toInt()
                txtStartTime.text =
                    (selectedListBeans!!.program_start_time!!.replace(":00:00", ":00"))
                txtEndTime.text = (selectedListBeans!!.program_end_time.replace(":00:00", ":00"))
                setTimerTask()
                setAlarmManager()
            }
        }
        /*if (selectedListBeans != null)
            radioManager = RadioManager.with(activity, selectedListBeans)
        else
            radioManager = RadioManager.with(activity)
        radioManager!!.bind()*/
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<String>,
        grantResults: IntArray
    ) {
        PermissionUtils.onRequestPermissionsResult(
            activity,
            requestCode,
            permissions,
            grantResults,
            object :
                PermissionUtils.permissionListener {
                override fun onDeny(requestCode: Int) {
                    showSnackBarSettings()
                }

                override fun onAllow(requestCode: Int) {
                }

                override fun onDenyNeverAskAgain(requestCode: Int) {
                    showSnackBarSettings()
                }

            })
    }

    private fun setTimerTask() {
        Handler().postDelayed({
            val completed = Utils.isTimeInbetween1(
                simpleDateFormat.format(getInstance().time),
                selectedListBeans!!.program_end_time
            )
            if (progress != null) {
                progress.progress = 100 - (completed / (totalProgramTime / 100)).toInt()
                setTimerTask()
            }
        }, 10000)
    }

    private fun setAlarmManager() {
        val alarmMgr = context!!.getSystemService(Context.ALARM_SERVICE) as AlarmManager
        val intent = Intent(context, NextProgramReceiver::class.java)
        val alarmIntent =
            PendingIntent.getBroadcast(
                context,
                0,
                intent,
                PendingIntent.FLAG_UPDATE_CURRENT
            )

        val calendar = getInstance()
        val temp = getInstance()
        temp.time = simpleDateFormat.parse(selectedListBeans!!.program_end_time)
        calendar.timeInMillis = System.currentTimeMillis()
        calendar[HOUR_OF_DAY] = temp.get(HOUR_OF_DAY)
        calendar[MINUTE] = temp.get(MINUTE)
        calendar[SECOND] = temp.get(SECOND)
        alarmMgr.setExact(AlarmManager.RTC_WAKEUP, calendar.timeInMillis, alarmIntent)
    }

    private fun setAdapter() {
        mPromotedProgramListAdapter = PromotedProgramListAdapter(
            context!!,
            mPromotedProgramsList, this
        )
        if (fragment_home != null)
            fragment_home.rvPromotedPrograms.adapter = mPromotedProgramListAdapter
    }


    override fun onClubItemClicked(data: PromotedProgramsList?) {
        val bundle = bundleOf(AppConstants.ID to data!!.id)
        val programDetailFragment = ProgramDetailFragment()
        programDetailFragment.arguments = bundle
        (activity as DashboardActivity).replaceFragment(programDetailFragment)
    }

    private fun getBroadcastScheduleList() {

        val params = HashMap<String, String>()
        params["method"] = "getBroadcastScheduleList"

        try {
            Retrofit.with(this.activity!!)
                .setAPI(APIs.BASE)
                .setGetParameters(params)
                .setCallBackListener(object :
                    JSONCallback(this.activity!!, showProgressBar()) {
                    override fun onFailed(statusCode: Int, message: String) {
                        hideProgressBar()
                        showShortToast(message)
                    }

                    override fun onSuccess(statusCode: Int, jsonObject: JSONObject) {
                        hideProgressBar()
                        if (jsonObject.has("error") && jsonObject.getInt("error") == 0 && jsonObject.has(
                                "list_broadcast"
                            )
                        ) {
                            val list_broadcast: JSONObject =
                                jsonObject.getJSONObject("list_broadcast")
                            val gson = Gson()
                            broadcastScheduleListModel =
                                gson.fromJson<BroadcastScheduleModel>(
                                    list_broadcast.toString(),
                                    BroadcastScheduleModel::class.java
                                )
                            session.storeBroadcastSchedule(broadcastScheduleListModel)
                            mTodaysProgramList = getData(broadcastScheduleListModel)
                            setTodaysAdapter()
                        }
                    }
                })
        } catch (e: Exception) {
            hideProgressBar()
        }
    }

    private fun getData(broadcastScheduleListModel: BroadcastScheduleModel?): ArrayList<ListBeans> {
        when (getInstance().get(DAY_OF_WEEK)) {
            1 -> return broadcastScheduleListModel!!.first!!
            2 -> return broadcastScheduleListModel!!.second!!
            3 -> return broadcastScheduleListModel!!.three!!
            4 -> return broadcastScheduleListModel!!.four!!
            5 -> return broadcastScheduleListModel!!.five!!
            6 -> return broadcastScheduleListModel!!.six!!
            7 -> return broadcastScheduleListModel!!.seven!!
        }
        return broadcastScheduleListModel!!.first!!
    }

    private fun getPromotedProgramsList() {

        val params = HashMap<String, String>()
        params["method"] = "getPromotedProgramsList"

        try {
            Retrofit.with(this.activity!!)
                .setAPI(APIs.BASE)
                .setGetParameters(params)
                .setCallBackListener(object :
                    JSONCallback(this.activity!!, null) {
                    override fun onFailed(statusCode: Int, message: String) {
//                        hideProgressBar()
                        showShortToast(message)
                    }

                    override fun onSuccess(statusCode: Int, jsonObject: JSONObject) {
//                        hideProgressBar()
                        if (jsonObject.has("list_programs")) {
                            val modelType =
                                object :
                                    TypeToken<ArrayList<PromotedProgramsList>>() {}.type
                            mPromotedProgramsList = Gson().fromJson(
                                jsonObject.getJSONArray("list_programs").toString(),
                                modelType
                            )
                            setAdapter()
                        }
                    }
                })
        } catch (e: Exception) {
//            hideProgressBar()
        }
    }

    override fun onStart() {
        super.onStart()
//        EventBus.getDefault().register(this)
    }

    override fun onStop() {
//        EventBus.getDefault().unregister(this)
        super.onStop()
    }

    override fun onDestroyView() {
        super.onDestroyView()
        /*if (radioManager != null)
            radioManager!!.unbind()*/
    }

    override fun onResume() {
        super.onResume()
//        radioManager.bind()
        if (RadioManager.getService() != null && !RadioManager.getService().isRadioOn) {
            setPauseButton()
        }
        if (RadioManager.getService() != null && RadioManager.getService().isRadioOn) {
            setPlayButton()
        }
    }

    /*@Subscribe
    fun onEvent(status: String) {
        when (status) {
            PlaybackStatus.LOADING -> {
            }
            PlaybackStatus.ERROR -> {
                if (!Utils.isConnectingToInternet(mContext!!)) {
                    showShortToast(getString(R.string.no_internet_connection))
                }
                Toast.makeText(
                    activity,
                    R.string.something_went_wrong,
                    Toast.LENGTH_SHORT
                ).show()
                RadioManager.getService().stopForeground(true)
            }
        }
        imgPlay.setImageResource(if (status == PlaybackStatus.PLAYING) R.drawable.ic_vector_pause else R.drawable.ic_vector_play)
        playEqualizer()
//        setScrollAppBar(status)
        (activity as DashboardActivity).setEqualizer(status)
    }*/

    private fun setScrollAppBar(status: String) {
        val layoutParams: AppBarLayout.LayoutParams =
            toolbar_layout.layoutParams as AppBarLayout.LayoutParams
        Handler().postDelayed({
            if (status == PlaybackStatus.PLAYING) {
                layoutParams.scrollFlags = (AppBarLayout.LayoutParams.SCROLL_FLAG_SCROLL
                        or AppBarLayout.LayoutParams.SCROLL_FLAG_EXIT_UNTIL_COLLAPSED)
                addOffsetListener()
            } else {
                app_bar.addOnOffsetChangedListener(null)
                layoutParams.scrollFlags = AppBarLayout.LayoutParams.SCROLL_FLAG_NO_SCROLL
                /*app_bar.setExpanded(true, true)
                ((app_bar.layoutParams as CoordinatorLayout.LayoutParams).behavior as AppBarLayout.Behavior).leftAndRightOffset =
                    0
                ((app_bar.layoutParams as CoordinatorLayout.LayoutParams).behavior as AppBarLayout.Behavior).topAndBottomOffset =
                    0*/
            }
        }, 500)

    }

    public fun setPlayer(status: String) {
        if (imgPlay != null)
            imgPlay.setImageResource(if (status == PlaybackStatus.PLAYING || status == PlaybackStatus.LOADING) R.drawable.xyz_new else R.drawable.xyz)
    }

    public fun playEqualizer() {
        try {
            Handler().postDelayed({
                if (RadioManager.getService().exoPlayer.playbackState == PlaybackState.STATE_PLAYING) {
                    if (equalizer_view != null)
                        equalizer_view.setAudioSessionId(RadioManager.getService().exoPlayer.audioSessionId)
                } else {
                    playEqualizer()
                }
            }, 1000)
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    fun playRadio(
    ) {
        if (!Utils.isConnectingToInternet(mContext!!)) {
            showShortToast(getString(R.string.no_internet_connection))
            return
        }
        /*if (selectedListBeans == null) {
            showShortToast(getString(R.string.no_program_available))
            return
        }*/
        PermissionUtils.isCheck(
            activity, arrayOf(
                PermissionUtils.RECORD_AUDIO
            ),
            PermissionUtils.PERMISSION_CODE, object : PermissionUtils.permissionListener {
                override fun onAllow(requestCode: Int) {
                    if (session != null && session.userDetail != null && session.userDetail!!.url_broadcast != null)
                        (activity as DashboardActivity).playRadio(
                            session.userDetail!!.url_broadcast.replace(
                                "https",
                                "http"
                            ), true, selectedListBeans, null, 0
                        )
                    else
                        getGeneralData()
                    /*if (radioManager != null)
                        radioManager!!.playOrPause(
                            session.userDetail.url_broadcast.replace(
                                "https",
                                "http"
                            )
                        )*/
                }

                override fun onDeny(requestCode: Int) {
                    showSnackBarSettings()
                }

                override fun onDenyNeverAskAgain(requestCode: Int) {
                    showSnackBarSettings()
                }
            }
        )
    }

    private fun getGeneralData() {
        if (!Utils.isConnectingToInternet(mContext!!)) {
            showShortToast(getString(R.string.no_internet_connection))
            return
        }
        val params = HashMap<String, String>()
        params["method"] = "getGeneralData"

        try {
            Retrofit.with(activity!!)
                .setAPI(APIs.BASE)
                .setGetParameters(params)
                .setCallBackListener(object :
                    JSONCallback(activity!!, null) {
                    override fun onFailed(statusCode: Int, message: String) {
                        showShortToast(message)
                    }

                    override fun onSuccess(statusCode: Int, jsonObject: JSONObject) {

                        val gson = Gson()
                        val generalDataModel =
                            gson.fromJson<GeneralDataModel>(
                                jsonObject.toString(),
                                GeneralDataModel::class.java
                            )
                        session.storeUserDetail(generalDataModel)
                    }
                })
        } catch (e: Exception) {
//            hideProgressBar()
        }
    }

    private fun showSnackBarSettings() {
        showSnackBar(coordinator,
            getString(R.string.please_grant_permission),
            Snackbar.LENGTH_INDEFINITE,
            getString(R.string.settings), object : OnSnackBarActionListener {
                override fun onAction() {
                    val intent = Intent()
                    intent.action = Settings.ACTION_APPLICATION_DETAILS_SETTINGS
                    intent.addCategory(Intent.CATEGORY_DEFAULT)
                    intent.data = Uri.parse("package:" + mContext!!.packageName)
                    intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
                    intent.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY)
                    intent.addFlags(Intent.FLAG_ACTIVITY_EXCLUDE_FROM_RECENTS)
                    startActivity(intent)
                }
            })
        /*MessageDialog(context).setMessage(context!!.getString(R.string.please_grant_permission))
            .cancelable(false)
            .setPositiveButton(
                getString(R.string.settings)
            ) { dialog, which ->
                dialog.dismiss()
                val intent = Intent()
                intent.action = Settings.ACTION_APPLICATION_DETAILS_SETTINGS
                intent.addCategory(Intent.CATEGORY_DEFAULT)
                intent.data = Uri.parse("package:" + mContext!!.packageName)
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
                intent.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY)
                intent.addFlags(Intent.FLAG_ACTIVITY_EXCLUDE_FROM_RECENTS)
                startActivity(intent)
            }.show()*/
    }

    fun setPauseButton() {
        if (imgPlay != null) {
            imgPlay.setImageResource(R.drawable.xyz)
        }
    }

    fun isPlaying(exoplayer: SimpleExoPlayer): Boolean {
        return exoplayer.playbackState == Player.STATE_READY && exoplayer.playWhenReady;
    }

    fun setPlayButton() {
        if (isPlaying(RadioManager.getService().exoPlayer)) {
            if (imgPlay != null) {
                imgPlay.setImageResource(R.drawable.xyz_new)
                playEqualizer()
            }
        }
    }
}
