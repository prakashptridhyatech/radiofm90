package com.shtibel.fm90.ui.programs

import android.content.Context
import android.graphics.Rect
import android.os.Bundle
import android.view.View
import androidx.annotation.DimenRes
import androidx.core.os.bundleOf
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.recyclerview.widget.RecyclerView.ItemDecoration
import com.google.android.exoplayer2.Player
import com.google.android.exoplayer2.SimpleExoPlayer
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import com.shtibel.fm90.Adapter.ProgramsListAdapter
import com.shtibel.fm90.R
import com.shtibel.fm90.activity.DashboardActivity
import com.shtibel.fm90.base.BaseFragment
import com.shtibel.fm90.interfaces.OnClubItemClicked
import com.shtibel.fm90.model.ListBeans
import com.shtibel.fm90.player.RadioManager
import com.shtibel.fm90.util.AppConstants
import com.shtibel.fm90.util.Utils
import com.shtibel.fm90.webservice.APIs
import com.shtibel.fm90.webservice.JSONCallback
import com.shtibel.fm90.webservice.Retrofit
import kotlinx.android.synthetic.main.fragment_programs.*
import org.json.JSONObject


class ProgramsFragment : BaseFragment(), OnClubItemClicked<ListBeans> {
    private lateinit var mScheduleListAdapter: ProgramsListAdapter
    private var mProgramsList = ArrayList<ListBeans>()
    private var selectedId = "";
    override fun setContentView(): Int = R.layout.fragment_programs


    override fun initView(rootView: View?, savedInstanceState: Bundle?) {
        rvPrograms.addItemDecoration(ItemOffsetDecoration(activity!!, R.dimen._6sdp))
        if (!RadioManager.getService().isRadioOn && isPlaying(RadioManager.getService().exoPlayer)) {
            selectedId = RadioManager.getService().listBeans.id
        }
        if (!Utils.isConnectingToInternet(mContext!!)) {
            showShortToast(getString(R.string.no_internet_connection))
        } else {
            getProgramsList()
        }
    }

    override fun setListeners() {
        swipeRefresh.setOnRefreshListener {
            swipeRefresh.isRefreshing = false
            if (!Utils.isConnectingToInternet(mContext!!)) {
                showShortToast(getString(R.string.no_internet_connection))
            } else {
                getProgramsList()
            }
        }
    }

    private fun setAdapter() {
        mScheduleListAdapter = ProgramsListAdapter(
            context!!,
            mProgramsList,
            this,
            selectedId
        )
        rvPrograms.adapter = mScheduleListAdapter
    }

    override fun onClubItemClicked(data: ListBeans?) {
        val bundle = bundleOf(AppConstants.ID to data!!.id)
        val programDetailFragment = ProgramDetailFragment()
        programDetailFragment.arguments = bundle
        (activity as DashboardActivity).replaceFragment(programDetailFragment)
    }

    private fun getProgramsList() {

        val params = HashMap<String, String>()
        params["method"] = "getProgramsList"

        try {
            Retrofit.with(this.activity!!)
                .setAPI(APIs.BASE)
                .setGetParameters(params)
                .setCallBackListener(object :
                    JSONCallback(this.activity!!, showProgressBar()) {
                    override fun onFailed(statusCode: Int, message: String) {
                        hideProgressBar()
                        showShortToast(message)
                    }

                    override fun onSuccess(statusCode: Int, jsonObject: JSONObject) {
                        hideProgressBar()
                        if (jsonObject.has("list_programs")) {
                            val modelType =
                                object :
                                    TypeToken<ArrayList<ListBeans>>() {}.type
                            mProgramsList = Gson().fromJson(
                                jsonObject.getJSONArray("list_programs").toString(),
                                modelType
                            )
                            setAdapter()
                        }
                    }
                })
        } catch (e: Exception) {
            hideProgressBar()
        }
    }

    class ItemOffsetDecoration(private val mItemOffset: Int) : ItemDecoration() {

        constructor(context: Context, @DimenRes itemOffsetId: Int) : this(
            context.getResources().getDimensionPixelSize(
                itemOffsetId
            )
        ) {
        }

        override fun getItemOffsets(
            outRect: Rect, view: View, parent: RecyclerView,
            state: RecyclerView.State
        ) {
            super.getItemOffsets(outRect, view, parent, state)
            outRect[mItemOffset, mItemOffset, mItemOffset] = mItemOffset
        }

    }

    fun setCurrentItem() {
        if (!RadioManager.getService().isRadioOn && isPlaying(RadioManager.getService().exoPlayer)) {
            if (selectedId != RadioManager.getService().listBeans.id)
                selectedId = RadioManager.getService().listBeans.id
        } else {
            selectedId = ""
        }
        setAdapter()
    }

    fun isPlaying(exoplayer: SimpleExoPlayer): Boolean {
        return exoplayer.playbackState == Player.STATE_READY && exoplayer.playWhenReady;
    }
}
