package com.shtibel.fm90.ui.programs

import android.content.Intent
import android.media.MediaMetadataRetriever
import android.media.session.PlaybackState
import android.net.Uri
import android.os.AsyncTask
import android.os.Bundle
import android.os.Handler
import android.provider.Settings
import android.text.TextUtils
import android.util.Log
import android.view.View
import android.view.View.GONE
import android.view.View.VISIBLE
import android.widget.SeekBar
import android.widget.SeekBar.OnSeekBarChangeListener
import android.widget.TextView
import androidx.constraintlayout.widget.ConstraintLayout
import com.google.android.exoplayer2.Player
import com.google.android.exoplayer2.SimpleExoPlayer
import com.google.android.material.shape.CornerFamily
import com.google.android.material.snackbar.Snackbar
import com.google.gson.Gson
import com.shtibel.fm90.Adapter.ProgramDetailListAdapter
import com.shtibel.fm90.R
import com.shtibel.fm90.activity.DashboardActivity
import com.shtibel.fm90.base.BaseFragment
import com.shtibel.fm90.interfaces.OnClubItemClicked
import com.shtibel.fm90.interfaces.OnSnackBarActionListener
import com.shtibel.fm90.model.ListBeans
import com.shtibel.fm90.model.ProgramArchiveData
import com.shtibel.fm90.model.ProgramDetailData
import com.shtibel.fm90.player.PlaybackStatus
import com.shtibel.fm90.player.RadioManager
import com.shtibel.fm90.util.AppConstants.ID
import com.shtibel.fm90.util.PermissionUtils
import com.shtibel.fm90.util.Utils
import com.shtibel.fm90.webservice.APIs
import com.shtibel.fm90.webservice.JSONCallback
import com.shtibel.fm90.webservice.Retrofit
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.activity_program_detail.*
import kotlinx.android.synthetic.main.layout_program_detail.view.*
import kotlinx.android.synthetic.main.toolbar_custom.view.*
import org.json.JSONObject
import java.text.SimpleDateFormat
import java.util.*
import java.util.concurrent.TimeUnit
import kotlin.collections.HashMap


class ProgramDetailFragment : BaseFragment(), OnClubItemClicked<Int> {
    private lateinit var mProgramDetailListAdapter: ProgramDetailListAdapter
    private lateinit var programData: ProgramDetailData
    override fun setContentView(): Int = R.layout.activity_program_detail
    private val handler: Handler = Handler()
    var id: String = ""
    var currentPlaying = -1;
    var endTimeTask: EndTimeTask? = null
    var status: String = ""
    override fun initView(rootView: View?, savedInstanceState: Bundle?) {
        progress.isEnabled = false
        progress.setOnSeekBarChangeListener(object : OnSeekBarChangeListener {
            override fun onStopTrackingTouch(arg0: SeekBar) {
                Log.e("errorCheck", "VOL7: ")
            }

            override fun onStartTrackingTouch(arg0: SeekBar) {
                Log.e("errorCheck", "VOL8: ")
            }

            override fun onProgressChanged(
                arg0: SeekBar,
                progress: Int,
                arg2: Boolean
            ) { // code here
                if (arg2) {
                    if (!RadioManager.getService().isRadioOn && RadioManager.getService().listBeans.id == id) {
                        handler.removeCallbacks(notification)
                        RadioManager.getService()
                            .exoPlayer.seekTo((RadioManager.getService().exoPlayer.duration * progress) / 100)
                        txtStartTime.text =
                            returnTime(RadioManager.getService().exoPlayer.currentPosition)
                    }
                }
            }
        })
        toolbar_custom.imgBack.setOnClickListener {
            activity!!.onBackPressed()
        }
        imgPlayer.setOnClickListener {
            checkPermission()
        }
        imgPlayPrevious.setOnClickListener {
            if (!RadioManager.getService().isRadioOn && RadioManager.getService().listBeans.id == id) {
                if (currentPlaying != 0) {
                    currentPlaying = currentPlaying - 1
                    checkPermission()
                    setPlayerData()
                }
            }
        }
        imgPlayNext.setOnClickListener {
            if (!RadioManager.getService().isRadioOn && RadioManager.getService().listBeans.id == id) {
                if (currentPlaying != programData.program_archive.size - 1) {
                    currentPlaying = currentPlaying + 1
                    checkPermission()
                    setPlayerData()
                }
            }
        }
        val radius10: Float = resources.getDimension(com.intuit.sdp.R.dimen._20sdp)
        expandedImage.shapeAppearanceModel = expandedImage.shapeAppearanceModel
            .toBuilder()
            .setBottomLeftCorner(CornerFamily.ROUNDED, radius10)
            .setBottomRightCorner(CornerFamily.ROUNDED, radius10)
            .build()
        shadow.shapeAppearanceModel = expandedImage.shapeAppearanceModel
            .toBuilder()
            .setBottomLeftCorner(CornerFamily.ROUNDED, radius10)
            .setBottomRightCorner(CornerFamily.ROUNDED, radius10)
            .build()
        layout_program_detail.imgEmpty.shapeAppearanceModel = expandedImage.shapeAppearanceModel
            .toBuilder()
            .setBottomLeftCorner(CornerFamily.ROUNDED, radius10)
            .setBottomRightCorner(CornerFamily.ROUNDED, radius10)
            .build()
        id = arguments!!.getString(ID, "")
        if (!RadioManager.getService().isRadioOn && RadioManager.getService().listBeans.id == id) {
            progress.isEnabled = true
            currentPlaying = RadioManager.getService().currentPosition
            if (imgPlayer != null)
                imgPlayer.setImageResource(if (isPlaying(RadioManager.getService().exoPlayer)) R.drawable.ic_pause_png else R.drawable.ic_play_png)
            if (RadioManager.getService().exoPlayer.playbackState == PlaybackState.STATE_PLAYING)
                primarySeekBarProgressUpdater()
        }
        getProgramData(id)
    }

    fun isPlaying(exoplayer: SimpleExoPlayer): Boolean {
        return exoplayer.playbackState == Player.STATE_READY && exoplayer.playWhenReady;
    }

    private fun playNextPrevious() {
        if (programData.program_archive != null && programData.program_archive.isNotEmpty()) {
            if (programData.program_archive[currentPlaying].program_audio == null || programData.program_archive[currentPlaying].program_audio.isEmpty()) {
                showShortToast(getString(R.string.no_program_available))
                return
            }
            if (!progress.isEnabled)
                progress.isEnabled = true
            val listBeans = ListBeans(
                id,
                "",
                programData.program_name,
                "",
                "",
                programData.broadcaster_img,
                programData.broadcaster_name
            )
            (activity as DashboardActivity).playRadio(
                programData.program_archive[currentPlaying].program_audio,
                false,
                listBeans,
                programData.program_archive,
                currentPlaying
            )
        }
    }

    override fun setListeners() {

    }

    private fun setAdapter() {
        val currentplaying =
            if (!RadioManager.getService().isRadioOn && RadioManager.getService().listBeans.id == id) currentPlaying else -1
        if (programData != null && programData.program_archive != null)
            mProgramDetailListAdapter =
            ProgramDetailListAdapter(
                programData.program_archive,
                this, currentplaying, status
            )
        layout_program_detail.rvProgramMP3.adapter = mProgramDetailListAdapter
    }

    private fun getProgramData(programID: String) {

        val params = HashMap<String, String>()
        params["method"] = "getProgramData"
        params["programID"] = programID

        try {
            Retrofit.with(this.activity!!)
                .setAPI(APIs.BASE)
                .setGetParameters(params)
                .setCallBackListener(object :
                    JSONCallback(this.activity!!, showProgressBar()) {
                    override fun onFailed(statusCode: Int, message: String) {
                        hideProgressBar()
                        showShortToast(message)
                    }

                    override fun onSuccess(statusCode: Int, jsonObject: JSONObject) {
                        hideProgressBar()
                        if (jsonObject.has("program_data")) {
                            programData =
                                Gson().fromJson<ProgramDetailData>(
                                    jsonObject.getJSONObject("program_data").toString(),
                                    ProgramDetailData::class.java
                                )
                            setProgramDetailData()
                        }
                    }
                })
        } catch (e: Exception) {
            hideProgressBar()
        }
    }

    private fun setProgramDetailData() {
        toolbar_custom.tvTitle.text = (programData.program_name.replace("&quot;", "\""))
        if (!TextUtils.isEmpty(programData.broadcaster_name)) {
            layout_program_detail.txtProgramParticipants.text =
                (programData.broadcaster_name.replace("&quot;", "\""))
            layout_program_detail.txtProgramParticipants.visibility = VISIBLE
        } else {
            layout_program_detail.txtProgramParticipants.visibility = GONE
        }
        if (!TextUtils.isEmpty(programData.program_description)) {
            layout_program_detail.txtProgramDetail.text =
                (programData.program_description.replace("&quot;", "\"").replace("&nbsp;", " "))
            layout_program_detail.txtProgramDetail.visibility = VISIBLE
        } else {
            layout_program_detail.txtProgramDetail.visibility = GONE
        }
        if (!TextUtils.isEmpty(programData.broadcaster_img))
            Picasso.get().load(programData.broadcaster_img)
                .into(expandedImage)
        else
            expandedImage.setImageResource(R.drawable.splash_90fm_logo)

        if (programData.program_archive != null && programData.program_archive.isNotEmpty()) {
//            Collections.reverse(programData.program_archive)
            Collections.sort(programData.program_archive, byDate)
            if (currentPlaying == -1 || (currentPlaying == 0))
                currentPlaying = 0
            setPlayerData()
            setAdapter()
            layoutPlayer.visibility = VISIBLE
        } else {
            val llp =
                layout_program_detail.layoutDetail.layoutParams as ConstraintLayout.LayoutParams
            llp.setMargins(0, 0, 0, 0)
            layout_program_detail.layoutDetail.layoutParams = llp
            layoutPlayer.visibility = GONE
        }
    }

    val sdf = SimpleDateFormat("yyyy-MM-dd")

    val byDate =
        Comparator<ProgramArchiveData> { programArchiveData: ProgramArchiveData, programArchiveData1: ProgramArchiveData ->
            if (!TextUtils.isEmpty(programArchiveData.program_date) && !TextUtils.isEmpty(
                    programArchiveData1.program_date
                )
            )
                return@Comparator if (sdf.parse(programArchiveData.program_date) > sdf.parse(
                        programArchiveData1.program_date
                    )
                ) -1 else 1
            else
                return@Comparator -1
        };

    /*
    *  /*public int compare(ord 1:ProgramArchiveData ,ord2: ProgramArchiveData ) {
        Date d1 = null;
        Date d2 = null;
        try {
            d1 = sdf.parse(ord1.lastModifiedDate);
            d2 = sdf.parse(ord2.lastModifiedDate);
        } catch (ParseException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }


        return (d1.getTime() > d2.getTime() ? -1 : 1);     //descending
    //  return (d1.getTime() > d2.getTime() ? 1 : -1);     //ascending
    }*/*/
    private fun setPlayerData() {
        txtProgramName.text =
            (programData.program_archive[currentPlaying].program_name.replace("&quot;", "\""))
        if (programData.program_archive[currentPlaying].program_date != null && programData.program_archive[currentPlaying].program_date.isNotEmpty()) {
            val sdf = SimpleDateFormat("yyyy-MM-dd")
            val sdf1 = SimpleDateFormat("dd/MM/yyyy")
            val date =
                programData.program_archive[currentPlaying].program_date.replace("&quot;", "\"")
            val d = sdf.parse(date)
            txtDate.text = sdf1.format(d!!)
        }
        endTimeTask = EndTimeTask(txtEndTime)
        if (programData.program_archive[currentPlaying].program_audio != null && programData.program_archive[currentPlaying].program_audio.isNotEmpty()) {
            endTimeTask!!.execute(programData.program_archive[currentPlaying].program_audio)
        }
//        setEndTime(programData.program_archive[currentPlaying].program_audio)
    }

    override fun onDestroyView() {
        super.onDestroyView()
        if (endTimeTask != null)
            endTimeTask!!.cancel(true)
    }

    inner class EndTimeTask(private val textView: TextView) : AsyncTask<String, Void, Long>() {
        override fun doInBackground(vararg params: String?): Long {
            val retriever = MediaMetadataRetriever();
            retriever.setDataSource(params[0], HashMap<String, String>());
            val time = retriever.extractMetadata(MediaMetadataRetriever.METADATA_KEY_DURATION);
            return time.toLong()
        }

        override fun onPostExecute(result: Long?) {
            super.onPostExecute(result)
            this.textView.text = returnTime(result!!)
        }
    }

    public fun returnTime(millis: Long): String {
        return java.lang.String.format(
            "%02d:%02d:%02d", TimeUnit.MILLISECONDS.toHours(millis),
            TimeUnit.MILLISECONDS.toMinutes(millis) - TimeUnit.HOURS.toMinutes(
                TimeUnit.MILLISECONDS.toHours(
                    millis
                )
            ),
            TimeUnit.MILLISECONDS.toSeconds(millis) - TimeUnit.MINUTES.toSeconds(
                TimeUnit.MILLISECONDS.toMinutes(
                    millis
                )
            )
        )
    }

    /** Method which updates the SeekBar primary progress by current song playing position */
    private fun primarySeekBarProgressUpdater() {
        txtStartTime.text = returnTime(RadioManager.getService().exoPlayer.currentPosition)
        if (txtEndTime.text.length != 8) {
            val endTime = returnTime(RadioManager.getService().exoPlayer.duration)
            txtEndTime.text =
                if (endTime.length == 8) returnTime(RadioManager.getService().exoPlayer.duration) else "00:00"
        }
        progress.progress =
            ((RadioManager.getService().exoPlayer.currentPosition.toFloat() * 100) / RadioManager.getService().exoPlayer.duration).toInt()
        // This math construction give a percentage of "was playing"/"song length"
        handler.postDelayed(notification, 1000)
    }

    val notification = Runnable {
        primarySeekBarProgressUpdater()
    }

    override fun onDestroy() {
        super.onDestroy()
        handler.removeCallbacks(notification)
    }

    public fun setPlayer(status: String) {
        this.status = status;
        if (!RadioManager.getService().isRadioOn && RadioManager.getService().listBeans.id == id && currentPlaying != RadioManager.getService().currentPosition) {
            if (!progress.isEnabled)
                progress.isEnabled = true
            currentPlaying = RadioManager.getService().currentPosition
            setPlayerData()
        }
        if (!RadioManager.getService().isRadioOn && RadioManager.getService().listBeans.id == id) {
            if (!progress.isEnabled)
                progress.isEnabled = true
            if (imgPlayer != null)
                imgPlayer.setImageResource(if (status == PlaybackStatus.PLAYING || status == PlaybackStatus.LOADING) R.drawable.ic_pause_png else R.drawable.ic_play_png)
            setAdapter()
            primarySeekBarProgressUpdater()
        }
    }

    override fun onClubItemClicked(position: Int) {
        currentPlaying = position
        /*txtProgramName.text =
            (programData.program_archive[position].program_name.replace("&quot;", " "))
        if (programData.program_archive[position].program_date.isNotEmpty()) {
            val sdf = SimpleDateFormat("yyyy-MM-dd")
            val sdf1 = SimpleDateFormat("dd/MM/yyyy")
            val date = programData.program_archive[position].program_date.replace("&quot;", " ")
            val d = sdf.parse(date)
            txtDate.text = sdf1.format(d!!)
        }*/
        /*val listBeans = ListBeans(
            id,
            "",
            programData.program_name,
            "",
            "",
            programData.broadcaster_img,
            programData.broadcaster_name
        )
        (activity as DashboardActivity).playRadio(
            programData.program_archive[position].program_audio,
            false,
            listBeans,
            programData.program_archive,
            position
        )*/
        setPlayerData()
        checkPermission()
        setAdapter()
    }

    private fun checkPermission() {
        if (!Utils.isConnectingToInternet(mContext!!)) {
            showShortToast(getString(R.string.no_internet_connection))
            return
        }
        PermissionUtils.isCheck(
            activity, arrayOf(
                PermissionUtils.RECORD_AUDIO
            ),
            PermissionUtils.PERMISSION_CODE, object : PermissionUtils.permissionListener {
                override fun onAllow(requestCode: Int) {
                    playNextPrevious()
                }

                override fun onDeny(requestCode: Int) {
                    showSnackBarSettings()
                }

                override fun onDenyNeverAskAgain(requestCode: Int) {
                    showSnackBarSettings()
                }
            }
        )
    }

    private fun showSnackBarSettings() {
        showSnackBar(coordinator,
            getString(R.string.please_grant_permission),
            Snackbar.LENGTH_INDEFINITE,
            getString(R.string.settings), object : OnSnackBarActionListener {
                override fun onAction() {
                    val intent = Intent()
                    intent.action = Settings.ACTION_APPLICATION_DETAILS_SETTINGS
                    intent.addCategory(Intent.CATEGORY_DEFAULT)
                    intent.data = Uri.parse("package:" + mContext!!.packageName)
                    intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
                    intent.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY)
                    intent.addFlags(Intent.FLAG_ACTIVITY_EXCLUDE_FROM_RECENTS)
                    startActivity(intent)
                }
            })
    }
}