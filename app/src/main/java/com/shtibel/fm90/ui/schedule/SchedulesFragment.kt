package com.shtibel.fm90.ui.schedule

import android.os.Bundle
import android.view.View
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentPagerAdapter
import androidx.viewpager.widget.ViewPager
import com.google.gson.Gson
import com.shtibel.fm90.R
import com.shtibel.fm90.base.BaseFragment
import com.shtibel.fm90.model.BroadcastScheduleModel
import com.shtibel.fm90.util.SessionManager
import com.shtibel.fm90.webservice.APIs
import com.shtibel.fm90.webservice.JSONCallback
import com.shtibel.fm90.webservice.Retrofit
import kotlinx.android.synthetic.main.fragment_schedules.*
import org.json.JSONObject
import java.util.*
import kotlin.collections.ArrayList
import kotlin.collections.HashMap

class SchedulesFragment : BaseFragment() {
    private lateinit var broadcastScheduleListModel: BroadcastScheduleModel
    private lateinit var adapter: Adapter

    override fun setContentView(): Int = R.layout.fragment_schedules

    override fun initView(rootView: View?, savedInstanceState: Bundle?) {
        val session = SessionManager(mContext!!)
        if (session.broadcastScheduleListModel != null) {
            broadcastScheduleListModel =
                session.broadcastScheduleListModel!!
            setupViewPager(viewPager)
            tabs.setupWithViewPager(viewPager)
        } else {
            getBroadcastScheduleList()
        }

    }

    private fun setupViewPager(viewPager: ViewPager?) {
        viewPager!!.addOnPageChangeListener(object : ViewPager.OnPageChangeListener {
            override fun onPageScrollStateChanged(state: Int) {

            }

            override fun onPageScrolled(
                position: Int,
                positionOffset: Float,
                positionOffsetPixels: Int
            ) {

            }

            override fun onPageSelected(position: Int) {

            }
        })
        adapter = Adapter(
            childFragmentManager
        )

        adapter.addFragment(
            SchedulesListFragment.newInstance(broadcastScheduleListModel.first!!),
            "ראשון"
        )
        adapter.addFragment(
            SchedulesListFragment.newInstance(broadcastScheduleListModel.second!!),
            "שני"
        )
        adapter.addFragment(
            SchedulesListFragment.newInstance(broadcastScheduleListModel.three!!),
            "שלישי"
        )
        adapter.addFragment(
            SchedulesListFragment.newInstance(broadcastScheduleListModel.four!!),
            "רביעי"
        )
        adapter.addFragment(
            SchedulesListFragment.newInstance(broadcastScheduleListModel.five!!),
            "חמישי"
        )
        adapter.addFragment(
            SchedulesListFragment.newInstance(broadcastScheduleListModel.six!!),
            "שישי"
        )
        adapter.addFragment(
            SchedulesListFragment.newInstance(broadcastScheduleListModel.seven!!),
            "שבת"
        )
        viewPager?.adapter = adapter
        viewPager.post {
            viewPager.currentItem = Calendar.getInstance().get(Calendar.DAY_OF_WEEK) - 1
        }
    }

    private class Adapter(manager: FragmentManager) : FragmentPagerAdapter(manager) {
        val fragments = ArrayList<Fragment>()
        val titles = ArrayList<String>()
        override fun getItem(position: Int): Fragment = fragments.get(position)

        override fun getCount(): Int = fragments.size

        override fun getPageTitle(position: Int): CharSequence? = titles.get(position)

        fun addFragment(fragment: Fragment, title: String) {
            fragments.add(fragment)
            titles.add(title)
        }
    }

    override fun setListeners() {
    }

    public fun getBroadcastScheduleList() {

        val params = HashMap<String, String>()
        params["method"] = "getBroadcastScheduleList"

        try {
            Retrofit.with(this.activity!!)
                .setAPI(APIs.BASE)
                .setGetParameters(params)
                .setCallBackListener(object :
                    JSONCallback(this.activity!!, showProgressBar()) {
                    override fun onFailed(statusCode: Int, message: String) {
                        hideProgressBar()
                        showShortToast(message)
                    }

                    override fun onSuccess(statusCode: Int, jsonObject: JSONObject) {
                        hideProgressBar()
                        if (jsonObject.has("error") && jsonObject.getInt("error") == 0 && jsonObject.has(
                                "list_broadcast"
                            )
                        ) {
                            val list_broadcast: JSONObject =
                                jsonObject.getJSONObject("list_broadcast")
                            val gson = Gson()
                            broadcastScheduleListModel =
                                gson.fromJson<BroadcastScheduleModel>(
                                    list_broadcast.toString(),
                                    BroadcastScheduleModel::class.java
                                )
                            val session = SessionManager(mContext!!)
                            session.storeBroadcastSchedule(broadcastScheduleListModel)
                            setupViewPager(viewPager)
                            tabs.setupWithViewPager(viewPager)
                        }
                    }
                })
        } catch (e: Exception) {
            hideProgressBar()
        }
    }
}
