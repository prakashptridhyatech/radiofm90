package com.shtibel.fm90.activity

import android.content.ActivityNotFoundException
import android.content.Context
import android.content.Intent
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.net.Uri
import android.os.Bundle
import android.provider.Settings
import android.util.TypedValue
import android.view.*
import android.view.Gravity.NO_GRAVITY
import android.view.Gravity.START
import android.view.LayoutInflater
import android.view.View.*
import android.widget.ImageView
import android.widget.PopupWindow
import android.widget.TextView
import android.widget.Toast
import androidx.annotation.Dimension
import androidx.appcompat.widget.PopupMenu
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentPagerAdapter
import androidx.viewpager.widget.ViewPager
import com.google.android.material.snackbar.Snackbar
import com.google.android.material.tabs.TabLayout
import com.shtibel.fm90.BuildConfig
import com.shtibel.fm90.R
import com.shtibel.fm90.base.BaseActivity
import com.shtibel.fm90.interfaces.OnSnackBarActionListener
import com.shtibel.fm90.model.ListBeans
import com.shtibel.fm90.model.ProgramArchiveData
import com.shtibel.fm90.player.PlaybackStatus
import com.shtibel.fm90.player.RadioManager
import com.shtibel.fm90.ui.home.HomeFragment
import com.shtibel.fm90.ui.programs.ProgramDetailFragment
import com.shtibel.fm90.ui.programs.ProgramsFragment
import com.shtibel.fm90.ui.schedule.SchedulesFragment
import com.shtibel.fm90.util.PermissionUtils.*
import com.shtibel.fm90.util.SessionManager
import com.shtibel.fm90.util.Utils
import com.shtibel.fm90.webservice.APIs
import com.shtibel.fm90.webservice.JSONCallback
import com.shtibel.fm90.webservice.Retrofit
import kotlinx.android.synthetic.main.activity_dashboard.*
import kotlinx.android.synthetic.main.activity_program_detail.*
import org.greenrobot.eventbus.EventBus
import org.greenrobot.eventbus.Subscribe
import org.json.JSONObject
import java.io.File
import java.io.FileOutputStream
import java.io.OutputStream


class DashboardActivity : BaseActivity() {
    public lateinit var pagerAdapter: PagerAdapter
    private var lastSelectedPosition = 0;
    var radioManager: RadioManager? = null
    var isRadioOn = true
    private lateinit var session: SessionManager

    override fun initViews() {
        radioManager = RadioManager.with(this)

        pagerAdapter =
            PagerAdapter(supportFragmentManager)

        pagerAdapter.addFragment(HomeFragment(), getString(R.string.title_home))
        pagerAdapter.addFragment(SchedulesFragment(), getString(R.string.title_schedule))
        pagerAdapter.addFragment(ProgramsFragment(), getString(R.string.title_program_list))
//        pagerAdapter.addFragment(BlankFragment(), getString(R.string.title_program_list))
        view_pager?.adapter = pagerAdapter
        view_pager.offscreenPageLimit = 0
        view_pager.addOnPageChangeListener(object : ViewPager.OnPageChangeListener {
            override fun onPageScrollStateChanged(state: Int) {

            }

            override fun onPageScrolled(
                position: Int,
                positionOffset: Float,
                positionOffsetPixels: Int
            ) {

            }

            override fun onPageSelected(position: Int) {
                showArrowImage(position)
            }
        })

        tabs.post {
            tabs.setupWithViewPager(view_pager)
            tabs.addOnTabSelectedListener(object : TabLayout.OnTabSelectedListener {
                override fun onTabSelected(tab: TabLayout.Tab?) {
                    val frga = supportFragmentManager.findFragmentById(R.id.frame)
                    if (frga != null) {
                        supportFragmentManager.beginTransaction().remove(
                            supportFragmentManager.findFragmentById(
                                R.id.frame
                            )!!
                        ).commit()
                    }
                    setSelected(tab, R.color.bottom_navigation_selected)
                    /* if (view_pager.currentItem == 2) {
                         (pagerAdapter.getItem(view_pager.currentItem) as ProgramsFragment).setCurrentItem()
                     }*/
                }

                override fun onTabUnselected(tab: TabLayout.Tab?) {
                    setSelected(tab, R.color.colorPrimary)
                    /* if (view_pager.currentItem == 2) {
                         (pagerAdapter.getItem(view_pager.currentItem) as ProgramsFragment).setCurrentItem()
                     }*/
                }

                override fun onTabReselected(tab: TabLayout.Tab?) {
                    val frga = supportFragmentManager.findFragmentById(R.id.frame)
                    if (frga != null) {
                        supportFragmentManager.beginTransaction().remove(
                            supportFragmentManager.findFragmentById(
                                R.id.frame
                            )!!
                        ).commit()
                    }
                    /*if (view_pager.currentItem == 2) {
                        (pagerAdapter.getItem(view_pager.currentItem) as ProgramsFragment).setCurrentItem()
                    }*/
                }
            })
            // Iterate over all tabs and set the custom view
            // Iterate over all tabs and set the custom view
            for (i in 0 until tabs.tabCount) {
                val tab = tabs.getTabAt(i)
                var view: View = pagerAdapter.getTabView(i, this)
                tab!!.customView = view
                if (i == 0) {
                    tab.customView!!.setBackgroundColor(
                        ContextCompat.getColor(
                            this@DashboardActivity,
                            R.color.bottom_navigation_selected
                        )
                    )
                    tab.view.setBackgroundColor(
                        ContextCompat.getColor(
                            this@DashboardActivity,
                            R.color.bottom_navigation_selected
                        )
                    )
                    view = pagerAdapter.getTabView(i, this)
                    val textView: TextView = view.findViewById(R.id.title)
                    val imageView: ImageView = view.findViewById(R.id.icon)
                    imageView.setImageResource(R.drawable.ic_action_more)
                    textView.text = getString(R.string.title_more)

                    tab.view.viewTreeObserver.addOnGlobalLayoutListener {
                        view.layoutParams = ViewGroup.LayoutParams(tab.view.width, tab.view.height)
                        llMore.removeAllViews()
                        llMore.addView(view)
                        llMore.setOnClickListener { v ->
                            showPopUp1(v)
                        }
                    }
                }
            }
        }
        view_pager.addOnPageChangeListener(object : ViewPager.OnPageChangeListener {

            override fun onPageScrollStateChanged(state: Int) {
            }

            override fun onPageScrolled(
                position: Int,
                positionOffset: Float,
                positionOffsetPixels: Int
            ) {

            }

            override fun onPageSelected(position: Int) {
                if (view_pager.currentItem == 2) {
                    (pagerAdapter.getItem(view_pager.currentItem) as ProgramsFragment).setCurrentItem()
                }
            }

        })
//        getLastUpdateData()
    }

    override fun onBackPressed() {
        val frga = supportFragmentManager.findFragmentById(R.id.frame)
        if (frga != null) {
            supportFragmentManager.beginTransaction().remove(
                supportFragmentManager.findFragmentById(
                    R.id.frame
                )!!
            ).commit()
            tabs.getTabAt(lastSelectedPosition)!!.select()
        } else
            super.onBackPressed()
    }

    fun replaceFragment(fragment: Fragment) {
        lastSelectedPosition = tabs.selectedTabPosition
        tabs!!.getTabAt(2)!!.select()
        supportFragmentManager.beginTransaction().replace(R.id.frame, fragment).commit()
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        if (requestCode == PERMISSION_CODE_FILE) {
            onRequestPermissionsResult(
                this,
                requestCode,
                permissions,
                grantResults,
                object :
                    permissionListener {
                    override fun onDeny(requestCode: Int) {
                        showSnackBarSettings()
                    }

                    override fun onAllow(requestCode: Int) {
                        shareOnWhatsapp()
                    }

                    override fun onDenyNeverAskAgain(requestCode: Int) {
                        showSnackBarSettings()
                    }

                })
        } else {
            pagerAdapter.getItem(view_pager.currentItem)
                .onRequestPermissionsResult(requestCode, permissions, grantResults)
        }

    }

    private fun setSelected(tab: TabLayout.Tab?, color: Int) {
        tab!!.customView!!.setBackgroundColor(
            ContextCompat.getColor(
                this@DashboardActivity,
                color
            )
        )
        tab.view.setBackgroundColor(
            ContextCompat.getColor(
                this@DashboardActivity,
                color
            )
        )
    }

    fun setEqualizer(status: String) {
        tabs.post {
            if (status == PlaybackStatus.PLAYING) {
                equalizerViewHome.visibility = VISIBLE
                equalizerViewHome.animateBars()
            } else {
                equalizerViewHome.visibility = GONE
            }
            equalizerViewProgram.visibility = GONE

        }
    }

    fun setProgram(status: String) {
        tabs.post {
            if (status == PlaybackStatus.PLAYING) {
                equalizerViewProgram.visibility = VISIBLE
                equalizerViewProgram.animateBars()
            } else {
                equalizerViewProgram.visibility = GONE
            }
            equalizerViewHome.visibility = GONE

        }
        if (view_pager.currentItem == 2) {
            (pagerAdapter.getItem(view_pager.currentItem) as ProgramsFragment).setCurrentItem()
        }
    }

    private fun showArrowImage(position: Int) {
        when (position) {
            0 -> {
                ivArrowHome.visibility = VISIBLE
                ivArrowProgram.visibility = INVISIBLE
                ivArrowSchedule.visibility = INVISIBLE
            }
            1 -> {
                ivArrowHome.visibility = INVISIBLE
                ivArrowProgram.visibility = INVISIBLE
                ivArrowSchedule.visibility = VISIBLE
            }
            2 -> {
                ivArrowHome.visibility = INVISIBLE
                ivArrowProgram.visibility = VISIBLE
                ivArrowSchedule.visibility = INVISIBLE
            }

        }
    }

    override fun setListeners() {
    }

    override fun getLayoutResId() = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        requestWindowFeature(Window.FEATURE_NO_TITLE)
        window.setFlags(
            WindowManager.LayoutParams.FLAG_FULLSCREEN,
            WindowManager.LayoutParams.FLAG_FULLSCREEN
        );
        setContentView(R.layout.activity_dashboard)
    }

    class PagerAdapter(fm: FragmentManager?) :
        FragmentPagerAdapter(fm!!) {
        private val imageResId =
            intArrayOf(
                R.drawable.ic_action_home,
                R.drawable.ic_action_schedule,
                R.drawable.ic_action_program_list
            )

        public val mFragments: MutableList<Fragment> =
            ArrayList()
        private val mFragmentTitles: MutableList<String> = ArrayList()
        fun addFragment(
            fragment: Fragment,
            title: String
        ) {
            mFragments.add(fragment)
            mFragmentTitles.add(title)
        }

        fun getTabView(
            position: Int,
            context: Context
        ): View { // Given you have a custom layout in `res/layout/custom_tab.xml` with a TextView and ImageView
            val v: View =
                LayoutInflater.from(context).inflate(R.layout.custom_tab, null)
            val tv = v.findViewById<View>(R.id.title) as TextView
            tv.text = mFragmentTitles.get(position)
            val img =
                v.findViewById<View>(R.id.icon) as ImageView
            img.setImageResource(imageResId[position])
            return v
        }

        override fun getItem(position: Int): Fragment {
            return mFragments[position]
        }

        override fun getCount(): Int {
            return mFragments.size
        }

        override fun getPageTitle(position: Int): CharSequence {
            return mFragmentTitles[position]
        }
    }

    private fun showPopUp1(view: View) {
        val inflater =
            getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
        val viewPopupMenu = inflater.inflate(R.layout.dialog_more_options, null)
        viewPopupMenu.measure(
            MeasureSpec.makeMeasureSpec(0, MeasureSpec.UNSPECIFIED),
            MeasureSpec.makeMeasureSpec(0, MeasureSpec.UNSPECIFIED)
        )
        val myPopUpWindow = PopupWindow(
            viewPopupMenu,
            viewPopupMenu.measuredWidth,
            viewPopupMenu.measuredHeight,
            true
        )
        val location = IntArray(2)
        view.getLocationOnScreen(location)
        viewPopupMenu.setOnTouchListener(OnTouchListener { _, _ ->
            myPopUpWindow.dismiss()
            return@OnTouchListener true
        })
        myPopUpWindow.showAtLocation(
            view,
            START,
            location[0],
            location[1]
        )
        viewPopupMenu.findViewById<TextView>(R.id.txtAboutUs).setOnClickListener {
            myPopUpWindow.dismiss()
            startActivity(Intent(this@DashboardActivity, AboutUsActivity::class.java))
        }
        viewPopupMenu.findViewById<TextView>(R.id.txtContactUs).setOnClickListener {
            myPopUpWindow.dismiss()
            startActivity(Intent(this@DashboardActivity, ContactUsActivity::class.java))
        }
        viewPopupMenu.findViewById<TextView>(R.id.txtShareApp).setOnClickListener {
            checkPermission()
            myPopUpWindow.dismiss()
        }
        viewPopupMenu.findViewById<TextView>(R.id.txtRateApp).setOnClickListener {
            try {
                startActivity(
                    Intent(
                        Intent.ACTION_VIEW,
                        Uri.parse("market://details?id=$packageName")
                    )
                )
            } catch (e: ActivityNotFoundException) {
                startActivity(
                    Intent(
                        Intent.ACTION_VIEW,
                        Uri.parse("https://play.google.com/store/apps/details?id=$packageName")
                    )
                )
            }
            myPopUpWindow.dismiss()
        }
    }

    private fun checkPermission() {
        isCheck(
            this, arrayOf(
                WRITE_EXTERNAL_STORAGE
            ),
            PERMISSION_CODE_FILE, object : permissionListener {
                override fun onAllow(requestCode: Int) {
                    shareOnWhatsapp()
                }

                override fun onDeny(requestCode: Int) {
                    showSnackBarSettings()
                }

                override fun onDenyNeverAskAgain(requestCode: Int) {
                    showSnackBarSettings()
                }
            }
        )
    }

    private fun showSnackBarSettings() {
        showSnackBar(coordinator,
            getString(R.string.please_grant_permission),
            Snackbar.LENGTH_INDEFINITE,
            getString(R.string.settings), object : OnSnackBarActionListener {
                override fun onAction() {
                    val intent = Intent()
                    intent.action = Settings.ACTION_APPLICATION_DETAILS_SETTINGS
                    intent.addCategory(Intent.CATEGORY_DEFAULT)
                    intent.data = Uri.parse("package:$packageName")
                    intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
                    intent.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY)
                    intent.addFlags(Intent.FLAG_ACTIVITY_EXCLUDE_FROM_RECENTS)
                    startActivity(intent)
                }
            })
    }

    private fun shareOnWhatsapp() {
        session = SessionManager(this)
        val bitmap = BitmapFactory.decodeResource(getResources(), R.drawable.app_logo);
        val filepath = File("" + getExternalFilesDir(null) + "/" + getString(R.string.app_name))
        if (!filepath.exists()) {
            filepath.mkdir()
        }
        val out: OutputStream
        val file = File(filepath.absoluteFile, "share.png")
        if (!file.exists()) {
            file.createNewFile()
        }
        try {
            out = FileOutputStream(file);
            bitmap.compress(Bitmap.CompressFormat.PNG, 100, out);
            out.flush();
            out.close();
        } catch (e: Exception) {
            e.printStackTrace();
        }
        val bmpUri = Uri.parse("file://" + file.path);
        val shareIntent = Intent(Intent.ACTION_SEND);
        shareIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        shareIntent.putExtra(Intent.EXTRA_STREAM, bmpUri);
        shareIntent.putExtra(
            Intent.EXTRA_TEXT,
            " 90 רדיו תשעים  fm\n \n\n Android: \n" +
                    "https://play.google.com/store/apps/details?id=" + BuildConfig.APPLICATION_ID + "\n\n"
                    + "iPhone:\n" + session.userDetail!!.ios_app_link
        );
        shareIntent.setType("text/plain");
        startActivity(Intent.createChooser(shareIntent, getString(R.string.share_app)));
    }

    private fun showPopUp(view: View) {
        val popup =
            PopupMenu(this, view)
        val inflater = popup.menuInflater
        inflater.inflate(R.menu.menu_more, popup.menu)
        popup.setOnMenuItemClickListener { item: MenuItem? ->

            when (item!!.itemId) {
                R.id.action_about_us -> {
                    startActivity(Intent(this@DashboardActivity, AboutUsActivity::class.java))
                }
                R.id.action_contact_us -> {
                    startActivity(Intent(this@DashboardActivity, ContactUsActivity::class.java))

                }
                R.id.action_share_app -> {
                }
                R.id.action_rate_app -> {
                }
            }
            true
        }

        popup.show()
    }

    override fun onStart() {
        super.onStart()
        EventBus.getDefault().register(this)
    }

    override fun onStop() {
        EventBus.getDefault().unregister(this)
        super.onStop()
    }

    override fun onDestroy() {
        radioManager!!.unbind()
        super.onDestroy()
    }

    override fun onResume() {
        super.onResume()
        radioManager!!.bind()
    }

    @Subscribe
    fun onEvent(status: String) {
        when (status) {
            PlaybackStatus.LOADING -> {
            }
            PlaybackStatus.ERROR -> {
                if (!Utils.isConnectingToInternet(this)) {
                    showShortToast(getString(R.string.no_internet_connection))
                }
                Toast.makeText(
                    this,
                    R.string.something_went_wrong,
                    Toast.LENGTH_SHORT
                ).show()
                RadioManager.getService().stopForeground(true)
            }
        }
        if (!RadioManager.getService().isRadioOn) {
            isRadioOn = false
        }
        if (isRadioOn) setEqualizer(status) else setProgram(status)
        if (isRadioOn) {
            val fragment = pagerAdapter.mFragments[0] as HomeFragment
            fragment.setPlayer(status)
            fragment.playEqualizer()
        } else {
            val fragment = supportFragmentManager.findFragmentById(R.id.frame)
            if (fragment != null) {
                (fragment as ProgramDetailFragment).setPlayer(status)
            }
        }

    }

    fun playRadio(
        status: String,
        isRadioOn: Boolean,
        listBeans: ListBeans?,
        program_archive: List<ProgramArchiveData>?,
        position: Int
    ) {
        this.isRadioOn = isRadioOn
        radioManager!!.playOrPause(status, listBeans, isRadioOn, position, program_archive)
    }

    private fun getLastUpdateData() {

        val params = HashMap<String, String>()
        params["method"] = "getLastUpdateDate"

        try {
            Retrofit.with(this)
                .setAPI(APIs.BASE)
                .setGetParameters(params)
                .setCallBackListener(object :
                    JSONCallback(this, null) {
                    override fun onFailed(statusCode: Int, message: String) {
                    }

                    override fun onSuccess(statusCode: Int, jsonObject: JSONObject) {
                        if (jsonObject.has("error") && jsonObject.getInt("error") == 0) {
                            val updateDate: String = jsonObject.getString("update_date")

                            val sp = getSharedPreferences("OneSignal", Context.MODE_PRIVATE)
                            val uDate = sp.getString("update_date", "")
                            if (uDate != null && uDate != "" && updateDate != uDate) {
                            }
                        }
                    }
                })
        } catch (e: Exception) {
        }
    }
}