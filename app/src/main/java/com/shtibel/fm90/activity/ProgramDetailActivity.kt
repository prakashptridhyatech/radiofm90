package com.shtibel.fm90.activity

import com.google.android.material.shape.CornerFamily
import com.shtibel.fm90.Adapter.ProgramDetailListAdapter
import com.shtibel.fm90.R
import com.shtibel.fm90.base.BaseActivity
import com.shtibel.fm90.interfaces.OnClubItemClicked
import com.shtibel.fm90.model.ProgramArchiveData
import kotlinx.android.synthetic.main.activity_program_detail.*
import kotlinx.android.synthetic.main.layout_program_detail.view.*

class ProgramDetailActivity : BaseActivity(), OnClubItemClicked<Int> {
    private lateinit var mProgramDetailListAdapter: ProgramDetailListAdapter

    override fun initViews() {
        setAdapter()
        val radius10: Float = resources.getDimension(com.intuit.sdp.R.dimen._20sdp)
        expandedImage.setShapeAppearanceModel(
            expandedImage.getShapeAppearanceModel()
                .toBuilder()
                .setBottomLeftCorner(CornerFamily.ROUNDED, radius10)
                .setBottomRightCorner(CornerFamily.ROUNDED, radius10)
                .build()
        )
        layout_program_detail.imgEmpty.setShapeAppearanceModel(
            expandedImage.getShapeAppearanceModel()
                .toBuilder()
                .setBottomLeftCorner(CornerFamily.ROUNDED, radius10)
                .setBottomRightCorner(CornerFamily.ROUNDED, radius10)
                .build()
        )
    }

    override fun setListeners() {
    }

    override fun getLayoutResId(): Int = R.layout.activity_program_detail

    private fun setAdapter() {
        mProgramDetailListAdapter = ProgramDetailListAdapter(
            ArrayList(),
            this,0,"")
        layout_program_detail.rvProgramMP3.adapter = mProgramDetailListAdapter
    }

    override fun onClubItemClicked(`object`: Int?) {

    }
}
