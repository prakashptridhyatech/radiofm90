package com.shtibel.fm90.activity

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.os.Handler
import android.util.Log
import android.view.View
import android.view.View.VISIBLE
import android.view.Window
import android.view.WindowManager
import android.view.animation.AccelerateInterpolator
import android.view.animation.Animation
import android.view.animation.AnimationUtils
import android.view.animation.TranslateAnimation
import com.google.gson.Gson
import com.onesignal.OSSubscriptionObserver
import com.onesignal.OSSubscriptionStateChanges
import com.onesignal.OneSignal
import com.shtibel.fm90.BuildConfig
import com.shtibel.fm90.R
import com.shtibel.fm90.base.BaseActivity
import com.shtibel.fm90.model.GeneralDataModel
import com.shtibel.fm90.util.SessionManager
import com.shtibel.fm90.webservice.APIs
import com.shtibel.fm90.webservice.ApiInterface
import com.shtibel.fm90.webservice.JSONCallback
import com.shtibel.fm90.webservice.Retrofit
import kotlinx.android.synthetic.main.activity_splash.*
import okhttp3.*
import okhttp3.MediaType.Companion.toMediaTypeOrNull
import org.json.JSONObject
import retrofit2.Call
import retrofit2.adapter.rxjava.RxJavaCallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import java.io.IOException
import java.util.concurrent.TimeUnit


class SplashActivity : BaseActivity(), OSSubscriptionObserver {
    lateinit var session: SessionManager
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        requestWindowFeature(Window.FEATURE_NO_TITLE)
        window.setFlags(
            WindowManager.LayoutParams.FLAG_FULLSCREEN,
            WindowManager.LayoutParams.FLAG_FULLSCREEN
        )
        setContentView(R.layout.activity_splash)
    }

    override fun initViews() {
        equalizer_view.animateBars()
        setAlphaAnimation(txtSplash)
        //equalizer_view.setAudioSessionId()
//        getPromotedProgramsList()
        getGeneralData()
//        getBroadcastScheduleList()
//        getLastUpdateDate()
        /*ActivityCompat.requestPermissions(
            this,
            arrayOf(android.Manifest.permission.RECORD_AUDIO),
            100
        )*/
        /* val userID = OneSignal.getPermissionSubscriptionState().subscriptionStatus.userId
         Log.e("userID", userID)*/
        OneSignal.addSubscriptionObserver(this);
    }

    private fun gotoMainScreen() {
        Handler().postDelayed(
            {
                //                startActivity(Intent(this@SplashActivity, ComingSoonActivity::class.java))
                startActivity(Intent(this@SplashActivity, DashboardActivity::class.java))
//                startActivity(Intent(this@SplashActivity, MainActivity::class.java))
                overridePendingTransition(R.anim.fade_in, R.anim.fade_out)
                finish()
            }, 500
        )
    }

    override fun setListeners() {

    }

    override fun getLayoutResId(): Int = 0

    private fun inFromRightAnimation(): Animation? {
        val inFromRight: Animation = TranslateAnimation(
            Animation.RELATIVE_TO_PARENT, +1.0f,
            Animation.RELATIVE_TO_PARENT, 0.0f,
            Animation.RELATIVE_TO_PARENT, 0.0f,
            Animation.RELATIVE_TO_PARENT, 0.0f
        )
        inFromRight.duration = 1000
        inFromRight.interpolator = AccelerateInterpolator()
        return inFromRight
    }

    private fun setSloganAnimation() {
        Handler().postDelayed(
            {
                slogan.visibility = VISIBLE
                setLeftToRightAnimation(slogan)
                Handler().postDelayed(
                    {
                        headphones.visibility = VISIBLE
                        setPopAnimation(headphones)
                    }, 500
                )
            }, 500
        )
    }

    private fun setLeftToRightAnimation(v: View) {
        val fade =
            AnimationUtils.loadAnimation(this, R.anim.slide_left_to_right)
        v.startAnimation(fade)
    }

    /*fun setRightToLeftAnimation(v: View) {
        val fade =
            AnimationUtils.loadAnimation(this, R.anim.slide_left_to_right)
        v.startAnimation(fade)
    }*/

    private fun setAlphaAnimation(v: View) {
        val fade =
            AnimationUtils.loadAnimation(this, R.anim.fade_in)
        fade.setAnimationListener(object : Animation.AnimationListener {
            override fun onAnimationRepeat(animation: Animation?) {

            }

            override fun onAnimationEnd(animation: Animation?) {
                logo.visibility = VISIBLE
                logo.startAnimation(inFromRightAnimation())
                setSloganAnimation()
            }

            override fun onAnimationStart(animation: Animation?) {

            }
        })
        v.startAnimation(fade)
    }

    private fun setPopAnimation(v: View) {
        val pop =
            AnimationUtils.loadAnimation(this, R.anim.slide_in_down)
        pop.setAnimationListener(object : Animation.AnimationListener {
            override fun onAnimationRepeat(animation: Animation?) {

            }

            override fun onAnimationEnd(animation: Animation?) {
                gotoMainScreen()
            }

            override fun onAnimationStart(animation: Animation?) {

            }
        })
        v.startAnimation(pop)
    }

    private fun getGeneralData() {

        val params = HashMap<String, String>()
        params["method"] = "getGeneralData"

        try {
            Retrofit.with(this)
                .setAPI(APIs.BASE)
                .setGetParameters(params)
                .setCallBackListener(object :
                    JSONCallback(this@SplashActivity, null) {
                    override fun onFailed(statusCode: Int, message: String) {
//                        hideProgressBar()
                        showShortToast(message)
                    }

                    override fun onSuccess(statusCode: Int, jsonObject: JSONObject) {
//                        hideProgressBar()

                        val gson = Gson()
                        val generalDataModel =
                            gson.fromJson<GeneralDataModel>(
                                jsonObject.toString(),
                                GeneralDataModel::class.java
                            )
                        session = SessionManager(this@SplashActivity)
                        session.storeUserDetail(generalDataModel)
                    }
                })
        } catch (e: Exception) {
//            hideProgressBar()
        }
    }

    override fun onOSSubscriptionChanged(stateChanges: OSSubscriptionStateChanges?) {
//        Log.e("onOSSubscriptionChanged", stateChanges!!.to.userId)
        if (stateChanges != null && stateChanges.to != null && stateChanges.to.userId != null)
            makeCall(stateChanges!!.to.userId)
        else
            Log.e("onOSSubscriptionChanged", "nulluserid")
        /*     Log.e("userid", "" + stateChanges!!.to.userId)
        val userID = OneSignal.getPermissionSubscriptionState().subscriptionStatus.userId
        Log.e("userID", userID)*/
    }

    /*private fun setUserData(userId: String) {

        val params = HashMap<String, String>()
        params["method"] = "setUserData"

        try {
            val paramsJson = JSONObject()
            params["onesignal_id"] = userId
            params["device_type"] = "Android"
            params["app_version"] = "1.0"

            Retrofit.with(this)
                .setUrl(APIs.BASE)
                .setParameters(paramsJson)
                .setCallBackListener(object :
                    JSONCallback(this@SplashActivity, null) {
                    override fun onFailed(statusCode: Int, message: String) {
                        showShortToast(message)
                    }

                    override fun onSuccess(statusCode: Int, jsonObject: JSONObject) {
                        Log.e("response", jsonObject.toString())
                    }
                })
        } catch (e: Exception) {
            Log.e("response", e.message)
        }
    }*/

    private fun makeCall(userId: String) {
        val sp = getSharedPreferences("OneSignal", Context.MODE_PRIVATE)
        sp.edit().putString("onesignal_id", userId).commit()
        val call: Call<ResponseBody>

        val client = OkHttpClient.Builder()
            .connectTimeout(60, TimeUnit.SECONDS)
            .readTimeout(60, TimeUnit.SECONDS)
            .writeTimeout(60, TimeUnit.SECONDS)
            .addInterceptor(Retrofit.provideHttpLoggingInterceptor())
            .addInterceptor(object : Interceptor {
                @Throws(IOException::class)
                public override fun intercept(chain: Interceptor.Chain): Response {
                    val original = chain.request()
                    val request: Request
                    request =
                        chain.request().newBuilder().method(original.method, original.body)
                            .build()
                    return chain.proceed(request)
                }
            }).build()

        val APIInterface = retrofit2.Retrofit.Builder()
            .baseUrl(APIs.BASE_URL)
            .client(client).addCallAdapterFactory(RxJavaCallAdapterFactory.create())
            .addConverterFactory(GsonConverterFactory.create())
            .build().create<ApiInterface>(
                ApiInterface::class.java
            )

        val paramsJson = JSONObject()
        paramsJson.put("onesignal_id", userId)
        paramsJson.put("device_type", "Android")
        paramsJson.put("app_version", BuildConfig.VERSION_NAME)
        val bodyRequest =
            RequestBody.create("application/json".toMediaTypeOrNull(), paramsJson.toString())
        call = APIInterface.callPostMethod(APIs.BASE + "?method=setUserData", bodyRequest)
        call.enqueue(object :
            JSONCallback(this@SplashActivity, null) {
            override fun onFailed(statusCode: Int, message: String) {
                showShortToast(message)
            }

            override fun onSuccess(statusCode: Int, jsonObject: JSONObject) {
                Log.e("response", jsonObject.toString())
            }
        })
    }
}