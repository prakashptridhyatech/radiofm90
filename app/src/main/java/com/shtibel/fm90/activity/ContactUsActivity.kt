package com.shtibel.fm90.activity

import android.app.AlertDialog
import android.content.ActivityNotFoundException
import android.content.Context
import android.content.DialogInterface
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.view.Window
import android.view.WindowManager
import android.widget.ProgressBar
import androidx.appcompat.app.AppCompatActivity
import com.shtibel.fm90.R
import com.shtibel.fm90.dilog.ContactUsErrorDialog
import com.shtibel.fm90.util.SessionManager
import com.shtibel.fm90.util.ValidationUtils
import com.shtibel.fm90.webservice.APIs
import com.shtibel.fm90.webservice.ApiInterface
import com.shtibel.fm90.webservice.JSONCallback
import com.shtibel.fm90.webservice.Retrofit
import kotlinx.android.synthetic.main.activity_contact_us.*
import kotlinx.android.synthetic.main.toolbar_custom_more.view.*
import okhttp3.*
import okhttp3.MediaType.Companion.toMediaTypeOrNull
import org.json.JSONObject
import retrofit2.Call
import retrofit2.adapter.rxjava.RxJavaCallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import java.io.IOException
import java.util.concurrent.TimeUnit


class ContactUsActivity : AppCompatActivity() {
    private lateinit var session: SessionManager
    private fun initViews() {
        if (session.userDetail != null) {
            toolbar_custom_more.tvTitle.text = session.userDetail!!.contact_page.title
            txtAddress.text = session.userDetail!!.contact_page.address
            txtPhone.text = session.userDetail!!.contact_page.phone
            txtEmail.text = session.userDetail!!.contact_page.email
        }
    }

    fun setListeners() {
        toolbar_custom_more.imgBack.setOnClickListener {
            onBackPressed()
        }
        btnContactUs.setOnClickListener {
            validateContactUs()
        }
        txtAddress.setOnClickListener {
            /*     val map = "http://maps.google.co.in/maps?q=" + session.userDetail.contact_page.address
                 val intent = Intent(Intent.ACTION_VIEW, Uri.parse(map))
                 startActivity(intent)*/

            try {
                // Launch Waze to look for Hawaii:
                var address: String = ""
                if (session.userDetail != null) {
                    address = session.userDetail!!.contact_page.address
                }
                val url = "https://waze.com/ul?q=" + address
                val intent = Intent(Intent.ACTION_VIEW, Uri.parse(url))
                startActivity(intent)
            } catch (ex: ActivityNotFoundException) {
                // If Waze is not installed, open it in Google Play:
                val intent = Intent(
                    Intent.ACTION_VIEW,
                    Uri.parse("market://details?id=com.waze")
                )
                startActivity(intent)
            }
        }
        txtPhone.setOnClickListener {
            val intent = Intent(Intent.ACTION_DIAL)
            var phone: String = ""
            if (session.userDetail != null) {
                phone = session.userDetail!!.contact_page.phone
            }
            intent.data = Uri.parse("tel:" + phone)
            startActivity(intent)
        }
        txtEmail.setOnClickListener {
            var email: String = ""
            if (session.userDetail != null) {
                email = session.userDetail!!.contact_page.email
            }
            val emailIntent = Intent(
                Intent.ACTION_SENDTO, Uri.fromParts(
                    "mailto", email, null
                )
            )
//            val email = Intent(Intent.ACTION_SEND)
//            emailIntent.type = "message/rfc822"
            /*email.putExtra(
                Intent.EXTRA_EMAIL,
                arrayOf(session.userDetail.contact_page.email)
            )*/
            startActivity(Intent.createChooser(emailIntent, "בחר לקוח דוא\"ל"))
        }
    }

    private fun validateContactUs() {
        if (edtOne.text.toString().isEmpty()) {
            showDialog(getString(R.string.empty_full_name), false)
        } else if (edtOne.text.toString().length < 2) {
            showDialog(getString(R.string.valid_full_name), false)
        } else if (edtTwo.text.toString().isEmpty()) {
            showDialog(getString(R.string.empty_phone), false)
        } else if (edtTwo.text.toString().length < 9) {
            showDialog(getString(R.string.valid_phone), false)
        } else if (edtThree.text.toString().isNotEmpty() && !ValidationUtils.isValidEmail(edtThree.text.toString())) {
            showDialog(getString(R.string.valid_email), false)
        } else if (edtFour.text.toString().isEmpty()) {
            showDialog(getString(R.string.empty_message), false)
        } else if (edtFour.text.toString().length < 9) {
            showDialog(getString(R.string.valid_message), false)
        } else {
            makeCall()
        }
    }

    private var progressDialog: AlertDialog? = null

    private fun isProgressOpen(): Boolean {
        return progressDialog != null && progressDialog!!.isShowing
    }

    private fun showProgressBar(
        context: Context,
        cancelable: Boolean,
        dismissListener: DialogInterface.OnDismissListener?
    ) {
        if (!isProgressOpen()) {
            progressDialog = AlertDialog.Builder(context, R.style.AppTheme_ProgressDialog)
                .setCancelable(cancelable)
                .setView(ProgressBar(context))
                .setOnDismissListener(dismissListener)
                .create()
        }
        progressDialog!!.show()
    }

    fun showProgressBar(): AlertDialog {
        showProgressBar(this, false, null)
        if (!isProgressOpen()) {
            progressDialog = AlertDialog.Builder(this, R.style.AppTheme_ProgressDialog)
                .setCancelable(false)
                .setView(ProgressBar(this))
                .create()
        }
        return progressDialog!!
    }

    fun hideProgressBar() {
        try {
            if (isProgressOpen()) {
                progressDialog!!.dismiss()
                progressDialog = null
            }
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    private fun makeCall() {
        val sp = getSharedPreferences("OneSignal", Context.MODE_PRIVATE)
        val userId = sp.getString("onesignal_id", "");
        val call: Call<ResponseBody>

        val client = OkHttpClient.Builder()
            .connectTimeout(60, TimeUnit.SECONDS)
            .readTimeout(60, TimeUnit.SECONDS)
            .writeTimeout(60, TimeUnit.SECONDS)
            .addInterceptor(Retrofit.provideHttpLoggingInterceptor())
            .addInterceptor(object : Interceptor {
                @Throws(IOException::class)
                public override fun intercept(chain: Interceptor.Chain): Response {
                    val original = chain.request()
                    val request: Request
                    request =
                        chain.request().newBuilder().method(original.method, original.body)
                            .build()
                    return chain.proceed(request)
                }
            }).build()

        val APIInterface = retrofit2.Retrofit.Builder()
            .baseUrl(APIs.BASE_URL)
            .client(client).addCallAdapterFactory(RxJavaCallAdapterFactory.create())
            .addConverterFactory(GsonConverterFactory.create())
            .build().create<ApiInterface>(
                ApiInterface::class.java
            )

        val paramsJson = JSONObject()
        paramsJson.put("onesignal_id", userId)
        paramsJson.put("full_name", edtOne.text.toString())
        paramsJson.put("phone", edtTwo.text.toString())
        paramsJson.put("email", edtThree.text.toString())
        paramsJson.put("message", edtFour.text.toString())
        val bodyRequest =
            RequestBody.create("application/json".toMediaTypeOrNull(), paramsJson.toString())
        call = APIInterface.callPostMethod(APIs.BASE + "?method=setContactUs", bodyRequest)
        showProgressBar()
        call.enqueue(object :
            JSONCallback(this, null) {
            override fun onFailed(statusCode: Int, message: String) {
                hideProgressBar()
                showDialog(message, false)
            }

            override fun onSuccess(statusCode: Int, jsonObject: JSONObject) {
                hideProgressBar()

                if (jsonObject.has("error") && jsonObject.getInt("error") == 0) {
                    showDialog("פנייתך נתקבלה בהצלחה.", true)
                } else if (jsonObject.has("error") && jsonObject.getInt("error") == 1) {
                    if (jsonObject.has("err_desc") && jsonObject.optString("err_desc") != null) {
                        showDialog(jsonObject.optString("err_desc"), false)
                    }
                }
            }
        })
    }

    private fun showDialog(message: String, isBack: Boolean) {
        ContactUsErrorDialog(this).setPositiveButton { dialog, which ->
            dialog.dismiss()
            if (isBack)
                onBackPressed()
        }.setMessage(message).show()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        requestWindowFeature(Window.FEATURE_NO_TITLE)
        window.setFlags(
            WindowManager.LayoutParams.FLAG_FULLSCREEN,
            WindowManager.LayoutParams.FLAG_FULLSCREEN
        )
        setContentView(R.layout.activity_contact_us)
        session = SessionManager(this@ContactUsActivity)
        initViews()
        setListeners()
    }

}
