package com.shtibel.fm90.activity

import android.os.Bundle
import android.text.Html
import android.view.Window
import android.view.WindowManager
import com.shtibel.fm90.R
import com.shtibel.fm90.base.BaseActivity
import com.shtibel.fm90.util.SessionManager
import kotlinx.android.synthetic.main.activity_about_us.*
import kotlinx.android.synthetic.main.toolbar_custom_more.view.*

class AboutUsActivity : BaseActivity() {
    override fun initViews() {
        val session = SessionManager(this@AboutUsActivity)
        if (session.userDetail != null) {
            toolbar_custom_more.tvTitle.text = session.userDetail!!.about_page.title
            txtAboutUs.text = Html.fromHtml(session.userDetail!!.about_page.text)
        }
    }

    override fun setListeners() {
        toolbar_custom_more.imgBack.setOnClickListener {
            onBackPressed()
        }
    }

    override fun getLayoutResId(): Int = 0
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        requestWindowFeature(Window.FEATURE_NO_TITLE)
        window.setFlags(
            WindowManager.LayoutParams.FLAG_FULLSCREEN,
            WindowManager.LayoutParams.FLAG_FULLSCREEN
        );
        setContentView(R.layout.activity_about_us)
    }
}