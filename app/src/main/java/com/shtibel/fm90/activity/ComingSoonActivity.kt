package com.shtibel.fm90.activity

import com.shtibel.fm90.R
import com.shtibel.fm90.base.BaseActivity

class ComingSoonActivity : BaseActivity() {
    override fun initViews() {
    }

    override fun setListeners() {
    }

    override fun getLayoutResId(): Int = R.layout.activity_coming_soon
}
