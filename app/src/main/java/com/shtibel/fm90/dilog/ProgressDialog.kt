package com.shtibel.fm90.dilog

import android.app.Dialog
import android.content.Context
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import com.shtibel.fm90.R
import com.shtibel.fm90.databinding.DialogProgressBinding
import java.util.*

class ProgressDialog(context: Context?, private val message: String? = null) :
    Dialog(context!!, R.style.ProgressDialog) {
    override fun onCreate(savedInstanceState: Bundle) {
        super.onCreate(savedInstanceState)
        val mBinder: DialogProgressBinding = DataBindingUtil.inflate(
            LayoutInflater.from(context),
            R.layout.dialog_progress,
            null,
            false
        )
        setContentView(mBinder.getRoot())
        setCanceledOnTouchOutside(false)
        setCancelable(false)
    }

    override fun onStart() {
        super.onStart()
        Objects.requireNonNull(window)
            ?.setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT)
        window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
    }

}