package com.shtibel.fm90.dilog;

import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;

import androidx.appcompat.app.AlertDialog;
import androidx.databinding.DataBindingUtil;

import com.shtibel.fm90.R;
import com.shtibel.fm90.databinding.DialogContactUsErrorBinding;

public class ContactUsErrorDialog extends AlertDialog implements View.OnClickListener {
    private String message;
    private OnClickListener onPositiveButtonClick;

    public ContactUsErrorDialog(Context context) {
        super(context, R.style.DialogWithAnimation);
    }


    public ContactUsErrorDialog setMessage(String message) {
        this.message = message;
        return this;
    }

    public ContactUsErrorDialog setPositiveButton(OnClickListener listener) {
        this.onPositiveButtonClick = listener;
        return this;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        DialogContactUsErrorBinding mBinder = DataBindingUtil.inflate(LayoutInflater.from(getContext()), R.layout.dialog_contact_us_error, null, false);
        setContentView(mBinder.getRoot());
        setCanceledOnTouchOutside(false);
        setCancelable(false);

        mBinder.tvMessage.setVisibility(message != null ? View.VISIBLE : View.GONE);
        if (message != null) mBinder.tvMessage.setText(message);

        mBinder.btnOk.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btnOk:
                if (onPositiveButtonClick != null)
                    onPositiveButtonClick.onClick(ContactUsErrorDialog.this, 0);
                break;
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
        if (getWindow() != null)
            getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
    }
}