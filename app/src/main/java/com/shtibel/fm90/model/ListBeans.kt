package com.shtibel.fm90.model

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class ListBeans(
    val id: String,
    val id_site_page_program: String,
    val program_name: String,
    val program_start_time: String?,
    val program_end_time: String,
    val broadcaster_img: String,
    val broadcaster_name: String
) : Parcelable