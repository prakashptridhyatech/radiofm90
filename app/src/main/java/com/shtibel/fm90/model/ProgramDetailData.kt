package com.shtibel.fm90.model

data class ProgramDetailData(
    val program_name: String,
    val program_description: String,
    val broadcaster_name: String,
    val broadcaster_img: String,
    val list_image: List<String>,
    var  program_archive: List<ProgramArchiveData>

)
