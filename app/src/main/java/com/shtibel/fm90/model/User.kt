package com.shtibel.fm90.model

class User (
     val id: Any,
     val name: String,
     val first_name: String,
     val last_name: String,
     val salutation: Any,
     val title: Any,
     val email: String,
     val mobile_number: Any,
     val member_since: Any,
     val date_of_birth: Any,
     val type: String,
     val email_verified_at: Any,
     val profile_photo: String,
     val address: Any,
     val city: Any,
     val zip: Any,
     val status: String,
     val created_at: String,
     val updated_at: String,
     val deleted_at: Any

)
