package com.shtibel.fm90.model

import com.google.gson.annotations.SerializedName
import java.util.*

class BroadcastScheduleModel {
    @SerializedName("ראשון")
    val first: ArrayList<ListBeans>? = null
    @SerializedName("שני")
    val second: ArrayList<ListBeans>? = null
    @SerializedName("שלישי")
    val three: ArrayList<ListBeans>? = null
    @SerializedName("רביעי")
    val four: ArrayList<ListBeans>? = null
    @SerializedName("חמישי")
    val five: ArrayList<ListBeans>? = null
    @SerializedName("שישי")
    val six: ArrayList<ListBeans>? = null
    @SerializedName("שבת")
    val seven: ArrayList<ListBeans>? = null
}
