package com.shtibel.fm90.model

data class AboutUsData(
    var title: String,
    var text: String
)