package com.shtibel.fm90.model

data class ProgramArchiveData(
    val program_name: String,
    val program_audio: String,
    val program_date: String
)
