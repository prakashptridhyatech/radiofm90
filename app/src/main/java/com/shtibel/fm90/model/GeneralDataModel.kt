package com.shtibel.fm90.model

data class GeneralDataModel(
    var site_logo: String,
    var url_broadcast: String,
    var android_app_link: String,
    var ios_app_link: String,
    var contact_page: ContactUsData,
    var about_page: AboutUsData
)