package com.shtibel.fm90.model

class ContactUsData(
    var title: String,
    var title_form: String,
    var address: String,
    var phone: String,
    var email: String
)