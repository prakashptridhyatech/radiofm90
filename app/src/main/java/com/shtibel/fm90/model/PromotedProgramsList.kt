package com.shtibel.fm90.model

data class PromotedProgramsList(
    val id: String,
    val program_name: String,
    val broadcaster_name: String,
    val broadcaster_img: String
)