package com.shtibel.fm90.equalizer;

import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.animation.ValueAnimator;
import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewTreeObserver;
import android.view.animation.LinearInterpolator;
import android.widget.ImageView;
import android.widget.LinearLayout;

import androidx.core.content.ContextCompat;

import com.shtibel.fm90.R;

public class EqualizerView extends LinearLayout {

    ImageView musicBar1, musicBar2, musicBar3, musicBar4, musicBar5, musicBar6, musicBar7, musicBar8,
            musicBar9, musicBar10, musicBar11, musicBar12, musicBar13, musicBar14, musicBar15, musicBar16;

    AnimatorSet playingSet;
    AnimatorSet stopSet;
    Boolean animating = false;

    Drawable foregroundColor;
    int duration;


    public EqualizerView(Context context) {
        super(context);
        initViews();
    }

    public EqualizerView(Context context, AttributeSet attrs) {
        super(context, attrs);
        setAttrs(context, attrs);
        initViews();
    }

    public EqualizerView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        setAttrs(context, attrs);
        initViews();
    }

    private void setAttrs(Context context, AttributeSet attrs) {
        TypedArray a = context.getTheme().obtainStyledAttributes(
                attrs,
                R.styleable.EqualizerView,
                0, 0);

        try {
//            foregroundColor = ContextCompat.getColor(getContext(), R.color.colorAccent);
            foregroundColor = ContextCompat.getDrawable(getContext(), R.drawable.equilizer);
//            foregroundColor = a.getInt(R.styleable.EqualizerView_foregroundColor, Color.BLACK);
            duration = a.getInt(R.styleable.EqualizerView_animDuration, 3000);

        } finally {
            a.recycle();
        }
    }

    private void initViews() {
        LayoutInflater.from(getContext()).inflate(R.layout.test_layout, this, true);
        musicBar1 = findViewById(R.id.music_bar1);
        musicBar2 = findViewById(R.id.music_bar2);
        musicBar3 = findViewById(R.id.music_bar3);
        musicBar4 = findViewById(R.id.music_bar4);
        musicBar5 = findViewById(R.id.music_bar5);
        musicBar6 = findViewById(R.id.music_bar6);
        musicBar7 = findViewById(R.id.music_bar7);
        musicBar8 = findViewById(R.id.music_bar8);

        musicBar9 = findViewById(R.id.music_bar9);
        musicBar10 = findViewById(R.id.music_bar10);
        musicBar11 = findViewById(R.id.music_bar11);
        musicBar12 = findViewById(R.id.music_bar12);
        musicBar13 = findViewById(R.id.music_bar13);
        musicBar14 = findViewById(R.id.music_bar14);
        musicBar15 = findViewById(R.id.music_bar15);
        musicBar16 = findViewById(R.id.music_bar16);
        setPivots();
    }


    private void setPivots() {
        setPivot(musicBar1);
        setPivot(musicBar2);
        setPivot(musicBar3);
        setPivot(musicBar4);
        setPivot(musicBar5);
        setPivot(musicBar6);
        setPivot(musicBar7);
        setPivot(musicBar8);
        setPivot(musicBar9);
        setPivot(musicBar10);
        setPivot(musicBar11);
        setPivot(musicBar12);
        setPivot(musicBar13);
        setPivot(musicBar14);
        setPivot(musicBar15);
        setPivot(musicBar16);
    }

    public void setPivot(View view) {
        view.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {
                if (view.getHeight() > 0) {
                    view.setPivotY(view.getHeight());

                    view.getViewTreeObserver().removeOnGlobalLayoutListener(this);

                }

            }
        });
    }

    public ObjectAnimator getAnimator1(View view) {
        ObjectAnimator scaleYbar1 = ObjectAnimator.ofFloat(view, "scaleY", 0.2f, 0.8f, 0.1f, 0.1f, 0.3f, 0.1f, 0.2f, 0.8f, 0.7f, 0.2f, 0.4f, 0.9f, 0.7f, 0.6f, 0.1f, 0.3f, 0.1f, 0.4f, 0.1f, 0.8f, 0.7f, 0.9f, 0.5f, 0.6f, 0.3f, 0.1f);
        scaleYbar1.setRepeatCount(ValueAnimator.INFINITE);
        return scaleYbar1;
    }

    public ObjectAnimator getAnimator2(View view) {
        ObjectAnimator scaleYbar2 = ObjectAnimator.ofFloat(view, "scaleY", 0.2f, 0.5f, 1.0f, 0.5f, 0.3f, 0.1f, 0.2f, 0.3f, 0.5f, 0.1f, 0.6f, 0.5f, 0.3f, 0.7f, 0.8f, 0.9f, 0.3f, 0.1f, 0.5f, 0.3f, 0.6f, 1.0f, 0.6f, 0.7f, 0.4f, 0.1f);
        scaleYbar2.setRepeatCount(ValueAnimator.INFINITE);

        return scaleYbar2;
    }

    public ObjectAnimator getAnimator3(View view) {
        ObjectAnimator scaleYbar3 = ObjectAnimator.ofFloat(view, "scaleY", 0.6f, 0.5f, 1.0f, 0.6f, 0.5f, 1.0f, 0.6f, 0.5f, 1.0f, 0.5f, 0.6f, 0.7f, 0.2f, 0.3f, 0.1f, 0.5f, 0.4f, 0.6f, 0.7f, 0.1f, 0.4f, 0.3f, 0.1f, 0.4f, 0.3f, 0.7f);
        scaleYbar3.setRepeatCount(ValueAnimator.INFINITE);
        return scaleYbar3;
    }

    public void animateBars() {
        animating = true;
        if (playingSet == null) {
            playingSet = new AnimatorSet();
            playingSet.playTogether(getAnimator1(musicBar16), getAnimator2(musicBar14), getAnimator3(musicBar15), getAnimator1(musicBar13), getAnimator2(musicBar11), getAnimator3(musicBar12), getAnimator1(musicBar10), getAnimator2(musicBar8), getAnimator3(musicBar9), getAnimator1(musicBar7), getAnimator2(musicBar5), getAnimator3(musicBar6), getAnimator1(musicBar4), getAnimator2(musicBar2), getAnimator3(musicBar3), getAnimator1(musicBar1));
            playingSet.setDuration(duration);
            playingSet.setInterpolator(new LinearInterpolator());
            playingSet.start();

        } else if (Build.VERSION.SDK_INT < 19) {
            if (!playingSet.isStarted()) {
                playingSet.start();
            }
        } else {
            if (playingSet.isPaused()) {
                playingSet.resume();
            }
        }

    }

    public void stopBars() {
        animating = false;
        if (playingSet != null && playingSet.isRunning() && playingSet.isStarted()) {
            if (Build.VERSION.SDK_INT < 19) {
                playingSet.end();
            } else {
                playingSet.pause();
            }
        }

        if (stopSet == null) {
            // Animate stopping bars
            stopSet = new AnimatorSet();
            stopSet.playTogether(getScalYAnimator(musicBar16), getScalYAnimator(musicBar15), getScalYAnimator(musicBar14), getScalYAnimator(musicBar13), getScalYAnimator(musicBar12), getScalYAnimator(musicBar11), getScalYAnimator(musicBar10), getScalYAnimator(musicBar9), getScalYAnimator(musicBar8), getScalYAnimator(musicBar7), getScalYAnimator(musicBar6), getScalYAnimator(musicBar5), getScalYAnimator(musicBar4), getScalYAnimator(musicBar3), getScalYAnimator(musicBar2), getScalYAnimator(musicBar1));
            stopSet.setDuration(200);
            stopSet.start();
        } else if (!stopSet.isStarted()) {
            stopSet.start();
        }
    }

    private ObjectAnimator getScalYAnimator(View view) {
        return ObjectAnimator.ofFloat(view, "scaleY", 1f);

    }

    public Boolean isAnimating() {
        return animating;
    }


}
