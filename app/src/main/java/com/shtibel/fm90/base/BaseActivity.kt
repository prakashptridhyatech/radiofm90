package com.shtibel.fm90.base

import android.app.Activity
import android.app.AlertDialog
import android.content.Context
import android.content.DialogInterface
import android.net.ConnectivityManager
import android.net.NetworkInfo
import android.os.Build
import android.os.Bundle
import android.view.View
import android.view.inputmethod.InputMethodManager
import android.widget.ProgressBar
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import com.google.android.material.snackbar.Snackbar
import com.shtibel.fm90.R
import com.shtibel.fm90.interfaces.OnSnackBarActionListener
import kotlinx.android.synthetic.main.toolbar_custom.*
import kotlinx.android.synthetic.main.toolbar_custom.view.*


abstract class BaseActivity : AppCompatActivity(), BaseView {
    private lateinit var mContext: Context
    private var progressDialog: AlertDialog? = null
    private var snackbar: Snackbar? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        mContext = this
        if (getLayoutResId() != 0) setContentView(getLayoutResId())
    }

    override fun setContentView(layoutResID: Int) {
        super.setContentView(layoutResID)
        initViews()
        setListeners()
    }

    abstract fun initViews()
    abstract fun setListeners()
    abstract fun getLayoutResId(): Int

    override fun hideKeyboard(activity: Activity?) {
        val imm = activity!!.getSystemService(Activity.INPUT_METHOD_SERVICE) as InputMethodManager
        var view = activity.currentFocus
        if (view == null) view = View(activity)
        imm.hideSoftInputFromWindow(view.windowToken, 0)
    }

    open fun showKeyboard(view: View) {
        if (view.requestFocus()) {
            val imm = getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
            imm.showSoftInput(view, InputMethodManager.SHOW_IMPLICIT)
        }
    }

    open fun showSnackBar(
        view: View?,
        msg: String?,
        LENGTH: Int,
        action: String?,
        actionListener: OnSnackBarActionListener?
    ) {
        if (view == null) return
        snackbar = Snackbar.make(view, msg!!, LENGTH)
        snackbar!!.setActionTextColor(
            ContextCompat.getColor(
                mContext!!,
                R.color.error_text_color
            )
        )
        if (actionListener != null) {
            snackbar!!.setAction(
                action,
                View.OnClickListener { view1: View? ->
                    snackbar!!.dismiss()
                    actionListener.onAction()
                }
            )
        }
        val sbView: View = snackbar!!.getView()
        val textView = sbView.findViewById<TextView>(R.id.snackbar_text)
        textView.setTextColor(ContextCompat.getColor(mContext!!, R.color.white))
        snackbar!!.show()
    }

    fun showSnackBar(view: View?, msg: String, LENGTH: Int) {
        if (view == null) return
        val snackbar = Snackbar.make(view, msg, LENGTH)
        val sbView = snackbar.view
        sbView.setBackgroundColor(ContextCompat.getColor(view.context, R.color.red))
        val textView = sbView.findViewById<TextView>(R.id.snackbar_text)
        textView.setTextColor(mContext.resources.getColor(R.color.white))
        snackbar.show()
    }

    override fun isNetworkConnected(): Boolean {
        val cm = getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        var networkInfo: NetworkInfo? = null
        networkInfo = cm.activeNetworkInfo
        return networkInfo != null && networkInfo.isConnected
    }

    fun showShortToast(message: String) {
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show()
    }

    fun showLongToast(message: String) {
        Toast.makeText(this, message, Toast.LENGTH_LONG).show()
    }

    fun hideProgressBar() {
        try {
            if (isProgressOpen()) {
                progressDialog!!.dismiss()
                progressDialog = null
            }
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    private fun isProgressOpen(): Boolean {
        return progressDialog != null && progressDialog!!.isShowing
    }

    fun showProgressBar(): AlertDialog {
        showProgressBar(mContext, false, null)
        if (!isProgressOpen()) {
            progressDialog = AlertDialog.Builder(mContext, R.style.AppTheme_ProgressDialog)
                .setCancelable(false)
                .setView(ProgressBar(mContext))
                .create()
        }
        return progressDialog!!
    }

    private fun showProgressBar(
        context: Context,
        cancelable: Boolean,
        dismissListener: DialogInterface.OnDismissListener?
    ) {
        if (!isProgressOpen()) {
            progressDialog = AlertDialog.Builder(context, R.style.AppTheme_ProgressDialog)
                .setCancelable(cancelable)
                .setView(ProgressBar(context))
                .setOnDismissListener(dismissListener)
                .create()
        }
        progressDialog!!.show()
    }

    override fun onResume() {
        super.onResume()
    }

    override fun onPause() {
        super.onPause()
    }

    override fun onDestroy() {
        super.onDestroy()
    }

    override fun attachBaseContext(base: Context?) {
        super.attachBaseContext(base)
    }

    open fun navigateToFragment(fragment: Fragment, addToBackstack: Boolean, tag: String?) {
        val fragmentManager = supportFragmentManager
        val fragmentTransaction = fragmentManager.beginTransaction()
        //        fragmentTransaction.setCustomAnimations(
        //            R.anim.enter_from_right, R.anim.exit_to_left,
        //            R.anim.enter_from_right, R.anim.exit_to_left
        //        )
//        fragmentTransaction.replace(R.id.frameContainerFragment, fragment, tag)
        if (addToBackstack) fragmentTransaction.addToBackStack(tag)
        fragmentTransaction.commit()
    }

    open fun getVisibleFragment(): Fragment? {
        val fragmentManager: FragmentManager = this.supportFragmentManager
        val fragments = fragmentManager.fragments
        for (fragment in fragments) {
            if (fragment != null && fragment.isVisible) return fragment
        }
        return null
    }

    open fun clearBackStack() {
        for (i in 0 until supportFragmentManager.backStackEntryCount) {
            supportFragmentManager.popBackStack()
        }
    }

    open fun setupToolBarWithBackArrowDetails(title: String) {

        toolbar.imgBack.setOnClickListener { super.onBackPressed() }
        toolbar.tvTitle.text = title


    }

    open fun popScreen() {
        supportFragmentManager.popBackStackImmediate()
    }

    open fun refreshFragment(fragment: Fragment) {
        val fragmentManager = supportFragmentManager
        val fragmentTransaction = fragmentManager.beginTransaction()
        if (Build.VERSION.SDK_INT >= 26) {
            fragmentTransaction.setReorderingAllowed(false);
        }
        fragmentTransaction.detach(fragment)
        fragmentTransaction.attach(fragment)
        fragmentTransaction.commitAllowingStateLoss()
    }
}