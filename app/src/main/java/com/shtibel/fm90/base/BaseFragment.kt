package com.shtibel.fm90.base

import android.app.Activity
import android.app.AlertDialog
import android.app.Dialog
import android.content.Context
import android.content.DialogInterface
import android.content.pm.PackageManager
import android.net.ConnectivityManager
import android.net.NetworkInfo
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.provider.MediaStore
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.InputMethodManager
import android.widget.ProgressBar
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import com.google.android.material.snackbar.Snackbar
import com.shtibel.fm90.R
import com.shtibel.fm90.interfaces.OnSnackBarActionListener

abstract class BaseFragment : Fragment(), BaseView, View.OnClickListener {

    var mContext: Context? = null
    var mActivity: AppCompatActivity? = null
    var mView: View? = null
    var isViewInitiated = false
    var permissionListener: setPermissionListener? = null
    private var progressDialog: AlertDialog? = null
    private var dialog: Dialog? = null
    private var snackbar: Snackbar? = null

    /**
     * To set fragment layout
     *
     * @return Layout id
     */
    abstract fun setContentView(): Int

    /**
     * To init view
     *
     * @param rootView           Fragment root view
     * @param savedInstanceState SavedInstance
     */
    abstract fun initView(rootView: View?, savedInstanceState: Bundle?)

    /**
     * To set listeners of views or any other
     */
    abstract fun setListeners()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun isNetworkConnected(): Boolean {
        val cm = mActivity!!.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        var networkInfo: NetworkInfo? = null
        networkInfo = cm.activeNetworkInfo
        return networkInfo != null && networkInfo.isConnected
    }

    override fun hideKeyboard(activity: Activity?) {
        val imm = activity?.getSystemService(Activity.INPUT_METHOD_SERVICE) as InputMethodManager
        var view = activity.currentFocus
        if (view == null) view = View(activity)
        imm.hideSoftInputFromWindow(view.windowToken, 0)
    }

    open fun showKeyboard(view: View) {
        if (view.requestFocus()) {
            val imm =
                mContext!!.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
            imm.showSoftInput(view, InputMethodManager.SHOW_IMPLICIT)
        }
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        mContext = getContext()
        mActivity = getContext() as AppCompatActivity?
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return if (mView != null) {
            mView
        } else inflater.inflate(setContentView(), container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        mView = view
        if (!isViewInitiated) {
            initView(view, savedInstanceState)
            setListeners()
            isViewInitiated = true
        }
    }

    override fun onResume() {
        super.onResume()
    }

    override fun onStop() {
        super.onStop()
    }

    override fun onClick(view: View?) {
    }

    open fun showSnackBar(
        view: View?,
        msg: String?,
        LENGTH: Int,
        action: String?,
        actionListener: OnSnackBarActionListener?
    ) {
        if (view == null) return
        snackbar = Snackbar.make(view, msg!!, LENGTH)
        snackbar!!.setActionTextColor(
            ContextCompat.getColor(
                mContext!!,
                R.color.error_text_color
            )
        )
        if (actionListener != null) {
            snackbar!!.setAction(
                action,
                View.OnClickListener { view1: View? ->
                    snackbar!!.dismiss()
                    actionListener.onAction()
                }
            )
        }
        val sbView: View = snackbar!!.getView()
        val textView = sbView.findViewById<TextView>(R.id.snackbar_text)
        textView.setTextColor(ContextCompat.getColor(mContext!!, R.color.white))
        snackbar!!.show()
    }

    fun requestAppPermissions(
        requestedPermissions: Array<String>,
        requestCode: Int,
        listener: setPermissionListener
    ) {
        this.permissionListener = listener
        var permissionCheck = PackageManager.PERMISSION_GRANTED
        for (permission in requestedPermissions) {
            permissionCheck =
                ContextCompat.checkSelfPermission(requireActivity(), permission)

            if (permissionCheck != PackageManager.PERMISSION_GRANTED && shouldShowRequestPermissionRationale(
                    permission
                )
            ) {
                ActivityCompat.requestPermissions(
                    requireActivity(),
                    requestedPermissions,
                    requestCode
                )
                return
            } else if (permissionCheck == PackageManager.PERMISSION_GRANTED) {
                permissionListener?.onPermissionGranted(requestCode)
                return
            } else if (permissionCheck != PackageManager.PERMISSION_GRANTED && !shouldShowRequestPermissionRationale(
                    permission
                )
            ) {
                permissionListener?.onPermissionDenied(requestCode)
                return
            } else if (Build.VERSION.SDK_INT >= 23 && !shouldShowRequestPermissionRationale(
                    permission
                )
            ) {
                permissionListener?.onPermissionDenied(requestCode)
                return
            } else {
                permissionListener?.onPermissionGranted(requestCode)
                return
            }
        }
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<String>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        var permissionCheck = PackageManager.PERMISSION_GRANTED
        for (permission in grantResults) {
            permissionCheck += permission
        }
        if (grantResults.isNotEmpty() && permissionCheck == PackageManager.PERMISSION_GRANTED) {
            permissionListener?.onPermissionGranted(requestCode)
        } else if (Build.VERSION.SDK_INT >= 23 && !shouldShowRequestPermissionRationale(permissions[0])) {
            permissionListener?.onPermissionDenied(requestCode)
        } else {
            permissionListener?.onPermissionDenied(requestCode)
        }
    }

    interface setPermissionListener {
        fun onPermissionGranted(requestCode: Int)
        fun onPermissionDenied(requestCode: Int)
    }

    fun showShortToast(message: String) {
        Toast.makeText(context, message, Toast.LENGTH_SHORT).show()
    }

    fun showLongToast(message: String) {
        Toast.makeText(context, message, Toast.LENGTH_LONG).show()
    }

    fun hideProgressBar() {
        try {
            if (isProgressOpen()) {
                progressDialog!!.dismiss()
                progressDialog = null
            }
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    private fun isProgressOpen(): Boolean {
        return progressDialog != null && progressDialog!!.isShowing
    }

    fun showProgressBar(): AlertDialog {
        showProgressBar(mContext!!, false, null)
        if (!isProgressOpen()) {
            progressDialog = AlertDialog.Builder(mContext, R.style.AppTheme_ProgressDialog)
                .setCancelable(false)
                .setView(ProgressBar(mContext))
                .create()
        }
        return progressDialog!!
    }

    private fun showProgressBar(
        context: Context,
        cancelable: Boolean,
        dismissListener: DialogInterface.OnDismissListener?
    ) {
        if (!isProgressOpen()) {
            progressDialog = AlertDialog.Builder(context, R.style.AppTheme_ProgressDialog)
                .setCancelable(cancelable)
                .setView(ProgressBar(context))
                .setOnDismissListener(dismissListener)
                .create()
        }
        progressDialog!!.show()
    }

    fun getRealPathFromURI(mContext: Context, contentURI: Uri): String? {
        val result: String?
        val filePathColumn = arrayOf(MediaStore.Images.ImageColumns.DATA)
        val cursor = mContext.contentResolver.query(contentURI, filePathColumn, null, null, null)
        if (cursor == null) {
            result = contentURI.path
        } else {
            cursor.moveToFirst()
            val idx = cursor.getColumnIndex(filePathColumn[0])
            result = cursor.getString(idx)
            cursor.close()
        }
        return result
    }


}