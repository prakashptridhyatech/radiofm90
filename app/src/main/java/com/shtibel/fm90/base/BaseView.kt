package com.shtibel.fm90.base

import android.app.Activity

interface BaseView {
    fun isNetworkConnected(): Boolean
    fun hideKeyboard(activity: Activity?)
}