package com.shtibel.fm90.interfaces;


public interface OnClubItemClicked<T> {
    void onClubItemClicked(T object);
}
