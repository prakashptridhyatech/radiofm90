package com.shtibel.fm90.interfaces;

/**
 * Created by viraj.patel on 10-Sep-18
 */
public interface OnRecyclerViewItemClicked<T> {

    void onacceptClicked(T object);
    void onRejecttClicked(T object);
}
