package com.shtibel.fm90.interfaces

interface OnSnackBarActionListener {
    fun onAction()
}